# -*- coding: utf-8 -*-
import os
import smtplib
from uuid import uuid4
from celery.task import task
from cStringIO import StringIO
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders

import settings
from util import Mail

import logging


@task(name='send_email', max_retries=0, ignore_result=True)
def send_email(mail):
    try:
        conn = smtplib.SMTP()
        conn.connect(settings.EMAIL_CONFIG['smtp_server'],
                     settings.EMAIL_CONFIG['smtp_port'])
        conn.ehlo()
        conn.starttls()
        conn.ehlo()
        conn.login(settings.EMAIL_CONFIG['smtp_username'],
                   settings.EMAIL_CONFIG['smtp_password'])

        conn.sendmail(
            settings.EMAIL_CONFIG['from'],
            mail._to + mail._cc + mail._bcc,
            mail.as_string()
        )
    except smtplib.SMTPException as exc:
        send_email.retry(exc=exc)


@task
def send_mail(subject, to, from_email, fname, body_message,
              pdf_name='requisitos'):
    multipart = MIMEMultipart()
    multipart['Subject'] = subject
    multipart['To'] = to
    multipart['From'] = from_email

    message = MIMEBase('application/pdf', None)
    message.set_payload(open(fname).read())
    message.add_header('Content-Disposition', 'attachment',
                       filename='%s.pdf' % pdf_name)
    encoders.encode_base64(message)

    multipart.attach(MIMEText(body_message.encode('utf8'), 'plain'))
    multipart.attach(message)

    conn = smtplib.SMTP()
    conn.connect(settings.EMAIL_CONFIG['smtp_server'],
                 settings.EMAIL_CONFIG['smtp_port'])
    conn.ehlo()
    conn.starttls()
    conn.ehlo()

    if pdf_name == 'el_credito_hipotecario_a_tu_medida':
        smpt_user = settings.EMAIL_CONFIG_PRODUCT['smtp_username']
        smpt_password = settings.EMAIL_CONFIG_PRODUCT['smtp_password']
    else:
        smpt_user = settings.EMAIL_CONFIG['smtp_username']
        smpt_password = settings.EMAIL_CONFIG['smtp_password']

    conn.login(smpt_user,smpt_password)

    conn.sendmail(
        multipart['From'],
        [multipart['To']],
        multipart.as_string()
    )

    conn.quit()
