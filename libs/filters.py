def format_date(value, format="%d/%m/%Y %H:%M:%S"):
    return value.strftime(format)


def bitwise_flag(value, flag):
    return (value & flag) == flag


def format_number(number, num_format='{0:,d}'):
    # Jode tener que crear un filtro para esto, jinja de mierda, en mako no
    # pasaba esto.
    return num_format.format(number)


def num_str(number):
    if number % 1:
        return '%.1f' % number
    return '%d' % number
