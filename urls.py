from tornado.web import url

import controllers.admin.project
import controllers.admin.auth
import controllers.site
import controllers.rest

urls = [
    url(
        r'/admin/?',
        controllers.admin.auth.Login,
        name="admin_"
    ),
    url(
        r'/admin/login',
        controllers.admin.auth.Login,
        name="admin_login"
    ),
    url(
        r'/admin/logout',
        controllers.admin.auth.Logout,
        name="admin_logout"
    ),
    url(
        r'/admin/projects/add',
        controllers.admin.project.AddOrEdit,
        name='admin_project_add'
    ),
    url(
        r'/admin/projects/externos/add',
        controllers.admin.project.AddOrEdit,
        {'externo': True},
        name='admin_project_add_externo'
    ),
    url(
        r'/admin/projects/edit/(.*)',
        controllers.admin.project.AddOrEdit,
        name='admin_project_edit'
    ),
    url(
        r'/admin/projects/delete',
        controllers.admin.project.Delete,
        name='admin_project_delete'
    ),
    url(
        r'/admin/projects/delete-image',
        controllers.admin.project.DeleteImage,
        name='admin_project_delete_image'
    ),
    url(
        r'/admin/projects',
        controllers.admin.project.List,
        name='admin_project_main'
    ),
    url(
        r'/admin/projects/externos',
        controllers.admin.project.List,
        {'externo': True},
        name='admin_project_main_externos'
    ),
    url(r'/contact', controllers.site.Contact),
    url(
        r'/quiero-comprar-una-casa/inmuebles',
        controllers.site.List,
        name='site_list_project'
    ),
    url(r'/departments',
        controllers.rest.Departments,
        name='departments'),
    url(r'/provinces/(\d{2})',
        controllers.rest.Provinces,
        name='provinces'),
    url(r'/districts/(\d{2})/(\d{2})',
        controllers.rest.Districts,
        name='districts'),
    url(
        r'/quiero-comprar-una-casa/ofertas-de-corredores-inmobiliarios',
        controllers.site.List,
        {'externo': True},
        name='site_list_project_externos',
    ),
    url(
        r'/quiero-comprar-una-casa/(.+)',
        controllers.site.Detail,
        name='site_det_project'
    ),
    url(r'/recommend', controllers.site.Recommend),
]
