$(function() {
  var bg = $('#bg');
  var _bg = { width: 1024, height: 655 };
  var bgd = { width: 1024, height: 655 };
  var ratio = getAspectRatio(1024, 636);
  var win = $(window);
  var doc = $(document);
  var html = $('#html');
  var body = $('#body');

  function getMCD(w, h) {
    return ((h !== 0) ? arguments.callee(h, w % h) : w);
  }

  function getAspectRatio(w, h) {
    var mcd = getMCD(w, h);

    return (w / mcd) / (h / mcd);
  }

  function apaisado(wi, he) {
    var r = {};

    r.wi = wi;
    r.he = wi / ratio;


    if (r.he < he) {
      r.wi = he * ratio;
      r.he = he;
    }

    return r;
  }

  function vertical(wi, he) {
    var r = {};

    r.wi = (he) * ratio;
    r.he = he;

    if (r.wi < wi) {
      r.wi = wi;
      r.he = wi / ratio;
    }
    return r;
  }

  function resize(scrollTop) {
    
    var wi = win.width();
    var he = win.height();

    body.css({
      width: (wi < 1061)? 1060: wi,
      height: (he < 742)? 742: he,
      overflow: 'hidden',
      position: 'relative'
    });

    he += (typeof scrollTop === 'number') ? scrollTop : win.scrollTop();

    var r = {};


    if (wi > he) {
      r = apaisado(wi, he);
    }

    else if (he > wi) {
      r = vertical(wi, he);
    }


    //bg.width(r.wi);
    //bg.height(r.he);


    // limite de ancho
    if (wi < 1070) {
      html.css({
        'overflow-x': 'auto',
        width: 1060
      });

      //body.width(1060).css({
        //overflow: 'hidden'
      //});

      bg.width(1060);
    }

    else {
      html.css({
        'overflow-x': 'hidden',
        width: 'auto'
      });

      html.scrollTop(0);
      //body.css({
        //width: 'auto'
      //});
      bg.width(r.wi);

    }

    // limite de alto
    if (he < 742) {
      html.css({
        'overflow-y': 'auto'
      });

      //body.height(742);
      bg.height(742);
    }

    else {
      html.css({
        'overflow-y': 'hidden',
        height: 'auto'
      });

      html.scrollTop(0);
      
      bg.height(r.he);

      //if (r.he > he) {
        //body.css({
          //height: he,
          //overflow: 'hidden'
        //});
      //}
      //else {
        //body.css({
          //height: 'auto',
          //overflow: 'auto'
        //});
      //}
    }
  }

  var timeout;
  win.bind('resize', function() {
    clearTimeout(timeout);
    timeout = setTimeout(resize, 200);
  });

  //if ($.browser.msie && ($.browser.version === '7.0' || $.browser.version === '8.0')) {
    timeout = setTimeout(function() {
      resize(doc.height() - win.height());
    }, 200);
  //}

  //else {
    //resize(doc.height() - win.height());
  //}


  $('.square').bind('mouseenter', function() {
    var el = $(this);
    el.find('.square-inner').animate({
      top: -85
    }, 300);
    el.find('.square-detail').animate({
      top: 0
    }, 300);
  }).bind('mouseleave', function() {
    var el = $(this);
    el.find('.square-inner').animate({
      top: 0
    }, 300);
    el.find('.square-detail').animate({
      top: 85
    }, 300);
  });
});
