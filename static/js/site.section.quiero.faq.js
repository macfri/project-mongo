$(function() {
  var list = {
  
    elements: {},

    open: function(e) {
      e.preventDefault();
      var el = $(this);
      var list = el.parent();
      var next = el.next();

      list.parent().find('.open').removeClass('open').find('.content').hide();

      if (next.is(':hidden')) {
        next.show();
        list.addClass('open');
      }

      else if (next.is(':visible')) {
        next.hide();
        list.removeClass('open');
      }
    },
  
    init: function() {
      var self = this;
      self.elements.lists = $('.list-down');
      self.elements.lists.find('.content').hide();
      self.elements.lists.find('.title').bind('click', self.open);
      
      //$('.show-next').bind('click', function(e) {
        //e.preventDefault();
        //var el = $(this);
        //el.parent().find('.show-next-open').removeClass('show-next-open');

        //if (el.hasClass('show-next-open')) {
          //el.removeClass('show-next-open');
          //el.next().removeClass('show-next-open');
        //}
        //else {
          //el.addClass('show-next-open');
          //el.next().addClass('show-next-open');
        //}
      //});

      return self;
    }
  }.init();


  var accordion_title = $('.accordion-title');

  accordion_title.bind('click', function(e) {
    e.preventDefault();

    var el = $(this);
    $('.accordion-title-open').removeClass('accordion-title-open').next().hide();
    el.addClass('accordion-title-open').next().show();
  });
});
