$(function() {
  var steps = {

    els: {},

    show: function(e) {
      e.preventDefault();
      var el = $(this);
      var transition = {
        ini: 'fadeIn',
        out: 'fadeOut'
      };

      if ($.browser.msie && parseInt($.browser.version,10) < 9) {
        transition.ini = 'show';
        transition.out = 'hide';
      }

      if (el.hasClass('hover')) {
        return;
      }

      $('.step-info:visible')[transition.out]();
      $('.hover').removeClass('hover');

      steps.els.steps.removeClass('active');
      $('.step:lt(' + el.index() + ')').addClass('active');

      $('#' + el.attr('data-show-id'))[transition.ini]();
      el.addClass('hover');

      if (el.is('#step-9')) {
        $('#step_info_0_1')[transition.ini]();
      }

      else {
        $('#step_info_0_1')[transition.out]();
      }
    },

    init: function() {
      var self = this;
      self.els.step_by = $('#step_by');
      self.els.steps = $('.step').bind('click', self.show);
      return self;
    }

  }.init();
});
