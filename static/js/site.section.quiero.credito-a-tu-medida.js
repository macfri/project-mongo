$(function() {
  window.questions = {
    config: {
      breadactive: 'current'
    },

    showNextQuestions: 1,

    elements: {},

    checkeds: {},

    _back: function(el) {
      var self = this;
      var index = el.index();
      var question = $('#' + el.attr('data-question-id'));

      if (
          index > -1 && index <
          self.elements.breadcrumbs.find('.item').length - 1
      ) {
        self.elements.breadcrumbs.find('.item').each(function(i, o) {
          if (i > index) {
            var el = $(this);

            $('#' +  el.attr('data-question-id'))
              .find('.checked')
              .removeClass('checked');

            el.remove();
            delete self.checkeds[el.attr('data-question-id')];
          }
        });
      }

      self.questionsShow({
        next: question
      });

      $('.question:gt(' + (question.index() - 1) + ')').each(function() {
        var el = $(this);
        el.find('.checked').removeClass('checked');
      });

      self.breadcrumbSelect.call(el);
    },

    back: function(e) {
      e.preventDefault();
      var el = $(this);
      var self = questions;
      var prev;

      if (
          self.elements.breadcrumbs.children().length > 1 &&
          $('#qfinal').is(':hidden')
      ) {
        prev = self.elements.breadcrumbs.find('.current').prev();
      }

      else {
        prev = self.elements.breadcrumbs.find('.current');
      }

      self._back(prev);

      if (prev.index() < 1) {
        el.hide();
      }
    },

    backto: function(e) {
      e.preventDefault();
      var el = $(e.target);
      var self = questions;

      if (
          el.length === 0 ||
          el.hasClass('item') === false
      ) {
        return;
      }

      self._back(el);
    },

    breadcrumbSelect: function() {
      var active = questions.config.breadactive;

      questions.elements.breadcrumbs.find('.' + active).removeClass(active);
      $(this).addClass(active);
    },

    breadcrumbFinalCheck: function() {
      var self = this;
      var last = self.elements.breadcrumbs.find('.final');

      if (last.length === 0) {
        last = self.elements.breadcrumbs.find('> :last');
      }

      if (last.is(':last') === false) {
        last.removeClass('final');
        self.elements.breadcrumbs.find('> :last').addClass('final');
      }
    },

    questionsShow: function(opt) {
      var self = this;
      var current = opt.current ||
          questions.elements.question.filter(':visible');
      var next = opt.next || current.next();

      if (self.showNextQuestions === 1) {
        current.fadeOut(500, function() {
          if (self.elements.toolbar.is(':hidden')) {
            self.elements.toolbar.show();
          }
          if (next.is('#qfinal')) {
            self.showCredito();
          }
          next.fadeIn(400);
        });
      }

      self.breadcrumbFinalCheck();
      //var self = this;
      //var current = opt.current ||
      //questions.elements.question.filter(':visible');
      //var next = opt.next || current.next();


      //current.fadeOut(500, function() {
      //if (self.elements.toolbar.is(':hidden')) {
      //self.elements.toolbar.show();
      //}
      //if (next.is('#qfinal')) {
      //self.showCredito();
      //}
      //next.fadeIn(400);
      //});

      //self.breadcrumbFinalCheck();
    },

    addCrumbs: function(obj) {
      var self = this;
      if (!self.checkeds[obj.question]) {
        self.elements.breadcrumbs.append(
            [
             '<span class="item" data-question-id="',
             obj.question,
             '">',
             obj.text,
             '</span>'
            ].join('')
        );
      }

      else {
        self.elements.breadcrumbs.find('.' +
            self.config.breadactive).html(obj.text);
      }

      self.breadcrumbFinalCheck();
    },

    add: function(obj) {
      var self = this;

      self.addCrumbs(obj);

      if (obj.next) {
        self.next(
            self.elements.question.filter('#' + obj.next)
        );
      }

      else {
        self.next();
      }

      self.checkeds[obj.question] = obj;

      //var self = this;

      //if (!self.checkeds[obj.question]) {
      //self.elements.breadcrumbs.append(
      //[
      //'<span class="item" data-question-id="',
      //obj.question,
      //'">',
      //obj.text,
      //'</span>'
      //].join('')
      //);
      //}

      //else {
      //self.elements.breadcrumbs.find('.' +
      //self.config.breadactive).html(obj.text);
      //}

      //self.breadcrumbFinalCheck();

      //if (obj.next) {
      //self.next(
      //self.elements.question.filter('#' + obj.next)
      //);
      //}

      //else {
      //self.next();
      //}

      //self.checkeds[obj.question] = obj;
    },

    next: function(next) {
      var self = this;
      var current = self.elements.question.filter(':visible');
      var bcurrent = self.elements.breadcrumbs.find('.' +
          self.config.breadactive);
      var bcurrent_index = bcurrent.index();

      if (bcurrent.length > 0) {
        bcurrent = bcurrent.next();
      }

      if (bcurrent.length === 0) {
        bcurrent = questions.elements.breadcrumbs.find(':last');
      }

      questions.breadcrumbSelect.call(bcurrent);

      questions.questionsShow({
        current: current,
        next: next || current.next()
      });
    },

    check: function(e) {
      e.preventDefault();
      var el = $(this);

      var question = el.parents('.question');
      question.find('.checked').removeClass('checked');
      el.addClass('checked');

      if (questions.elements.back.is(':hidden')) {
        questions.elements.back.show();
      }

      if (question.is('#qfinal')) {
      }

      else {
        questions.add({
          question: question.attr('id'),
          value: el.attr('data-value'),
          text: el.attr('data-text'),
          next: el.attr('data-next')
        });
      }
    },

    showCredito: function() {
      var self = this;

      /* hipotecario tradicional .check-1,
       * hipotecario pyme check-2
       * hipotecario con ahorro local check-3
       * hipotecario con remesas check-4
       * techo propio check-5
       * mivivienda check-6
       */

      self.q('qfinal').find('.radio-extend').hide().removeClass('radio-active');

      if (
          /* dependiente */
          self.getResponse('q1') === '1' ||
          self.getResponse('q2-3') === '1' ||

          /* Profiesional o tecnico */
          self.getResponse('q2-1') === '1' ||
          self.getResponse('q2-3') === '2' ||

          /* empresario consolidado */
          self.getResponse('q2-1') === '3' ||
          self.getResponse('q2-3') === '4' ||

          /* accionista de empresas */
          self.getResponse('q2-3') === '5' ||

          /* arrendador de propiedades */
          self.getResponse('q2-3') === '6'
      )
      {
        /* hipotecario tradicional */
        self.q('qfinal').find('.check-1').show().addClass('radio-active');

        /* mi vivienda y techo propio */
        if (
            self.getResponse('q4') === '2'
        )
        {
          self.q('qfinal').find('.check-6').show().addClass('radio-active');
          self.q('qfinal').find('.check-5').show().addClass('radio-active');

          self.q('figura44').hide().removeClass('radio-active');
          self.q('figura43').hide().removeClass('radio-active');

          if (self.getResponse('q5') === '2') {
            self.q('figura44').show().addClass('radio-active');
          }

          else {
            self.q('figura43').show().addClass('radio-active');
          }
        }
      }

      /* hipotecario pyme */
      if (
          self.getResponse('q2-1') === '2' ||
          self.getResponse('q2-3') === '3'
      )
      {
        self.q('qfinal').find('.check-2').show().addClass('radio-active');
      }

      /* hipotecario Ahorro local */
      if (
          self.getResponse('q2-2') === '1'
      )
      {
        self.q('qfinal').find('.check-3').show().addClass('radio-active');
        self.q('figura36').hide().removeClass('radio-active');
        self.q('figura35').hide().removeClass('radio-active');

        if (self.getResponse('q5') === '3') {
          self.q('figura35').show().addClass('radio-active');
        }

        else if (self.getResponse('q5') === '2') {
          self.q('figura36').show().addClass('radio-active');
        }
      }

      /* hipotecario remesas */
      if (
          self.getResponse('q2-2') === '2'
      )
      {
        self.q('qfinal').find('.check-4').show().addClass('radio-active');

        self.q('figura39').hide().removeClass('radio-active');
        self.q('figura40').hide().removeClass('radio-active');

        if (self.getResponse('q5') === '2') {
          self.q('figura39').show().addClass('radio-active');
        }

        else {
          self.q('figura40').show().addClass('radio-active');
        }
      }


      self.q('qfinal').find('.checks-group').each(function() {
        var el = $(this);
        if (el.find('.radio-active').length == 0) {
           el.hide();
         }
 
         else {
           el.show();
         }
      });

      var tmp = [];
      self.q('qfinal').find('.radio-extend').each(function() {
        var el = $(this);
        if (el.css('display') === 'table-cell') {
          tmp.push('1');
        }
      });

      if (tmp.length > 1) {
        self.q('qfinal').addClass('pair').removeClass('single');
      }

      else {
        self.q('qfinal').addClass('single').removeClass('pair');
      }
    },

    q: function(id) {
      var self = this;

      if (!self.elements[id]) {
        self.elements[id] = $('#' + id);
      }

      return self.elements[id];
    },

    getResponse: function(id) {
      var self = this;
      return self.q(id).find('.checked').attr('data-value');
    },

    getQuestionHash: function() {
      var self = this;
      var responses = [];
      var value;
      self.elements.question.each(function() {
        value = self.getResponse(this.id);

        if (this.id !== 'qfinal' && value) {
          responses.push(
              this.id + '_' + value
          );
        }
      });

      return responses;
    },

    init: function() {
      var self = this;
      self.elements.breadcrumbs = $('#questions_breadcrumbs');
      self.elements.toolbar = $('#questiontoolbar');
      self.elements.question = $('.question');
      self.elements.radio = $('.radio-extend').bind('click', self.check);
      self.elements.back = $('#backto').bind('click', self.back);

      self.elements.breadcrumbs.bind('click', self.backto);
      return self;
    }
  }.init();

  var autorelleno = {
    trigger: function(params) {
      params = params.split('-');
      var question = questions.q(params[0]);

      if (question.length > 0 && params[1]) {
        question.find('[data-value="' + params[1] + '"]').trigger('click');
      }
    },

    checked: function(params) {
      params = params.split('_');
      var question = questions.q(params[0]);
      var check;
      

      if (question.length > 0 && params[1]) {
        check = question.find('[data-value="' + params[1] + '"]').addClass('checked');
        questions.add({
          question: question.attr('id'),
          value: check.attr('data-value'),
          text: check.attr('data-text'),
          next: check.attr('data-next')
        });

        if (params[0] === 'qfinal') {
          $('#' + check.attr('data-reveal-id')).reveal({
            animation: 'fade'
          });
        }
      }


    },

    init: function() {
      var self = this;
      var query = location.hash;
      var params;
      var time = 1000;

      if (query) {
        params = query.split('/');
        params.shift();

        questions.showNextQuestions = 0;
        $.each(params, function(i, o) {
          self.checked(o);
        });
        questions.showCredito();
        questions.elements.question.hide();
        questions.elements.toolbar.show();
        $('#qfinal').show();
        questions.showNextQuestions = 1;
        if (questions.elements.back.is(':hidden')) {
          questions.elements.back.show();
        }
      }

      return self;
    }
  }.init();

  var enviar = {
    e: {},

    show: function(data) {
      var self = this;

      self.old = $('.reveal-open').trigger('reveal:close');

      self.e.email.val('');
      self.e.el.find('.error').hide();
      self.e.el.find('.text').hide();
      self.e.id.val(data.id||'');

      setTimeout(function() {
        self.e.el.reveal({
          animation: 'fade'
        });
      }, 700);
    },

    validate: function() {
      var self = this;
      var status = 0;
      self.e.el.find('.error').hide();

      if (self.e.email.val() === '') {
        status = 1;
        self.e.error2.show();
      }

      else if (
          !/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
        .test(self.e.email.val())
      )
      {
        status = 1;
        self.e.error1.show();
      }


      return status;
    },

    send: function(e) {
      e.preventDefault();

      var self = enviar;
      self.e.el.find('.text').hide();

      if (self.validate() !== 0) {
        return;
      }

      var responses = questions.getQuestionHash();
      var url = 'http://' + location.host + '/recommend';

      responses.push(
          'qfinal_' + $('#qfinal .radio-extend.checked').attr('data-value')
      );

      self.e.text1.show();

      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
          type: 'credito',
          link: responses.join('/'),
          email: self.e.email.val(),
          id: self.e.id.val()
        },
        success: function(r) {
          if (r.status_code === 0) {
            self.e.text1.hide();
            self.e.text2.show();
            self.e.email.val('');
          }
          else {
            self.e.el.find('.text').hide();
            self.e.error3.show();
          }
        },

        error: function() {
          self.e.el.find('.text').hide();
          self.e.error3.show();
        }
      });
    },

    init: function() {
      var self = this;
      self.e.el = $('#sendmail').bind('reveal:close', function() {
        setTimeout(function() {
          self.old.reveal({
            animation: 'fade'
          });
        }, 700);
      });

      self.e.form = $('#sendmail_form').bind('submit', self.send);
      self.e.email = $('#sendmail_email');
      self.e.id = $('#sendmail_id');
      self.e.error1 = $('#sendmail_error1');
      self.e.error2 = $('#sendmail_error2');
      self.e.error3 = $('#sendmail_error3');
      self.e.text1 = $('#sendmail_text1');
      self.e.text2 = $('#sendmail_text2');
      return self;
    }
  }.init();

  $('.revealopen').bind('click', function(e) {
    e.preventDefault();
    var el = $(this);
    var modal = el.parents('.reveal-modal');

    modal.trigger('reveal:close');

    setTimeout(function() {
      $('#' + el.attr('data-id')).reveal({
        animation: 'fade'
      });
    }, 700);
  });

  $('.printreq').bind('click', function(e) {
    e.preventDefault();
    var responses = [];
    var value;
    questions.elements.question.each(function() {
      value = questions.getResponse(this.id);

      if (this.id !== 'qfinal' && value) {
        responses.push(
            this.id + '_' + value
        );
      }

    });


    if (responses.length > 1) {
      location.href = '/quiero-comprar-una-casa/que-necesitas#!/' +
          responses.join('/');

      //window.open(
          //'http://' +
          //location.host +
          //'/quiero-comprar-una-casa/que-necesitas/#!/' +
          //responses.join('/'),
          //'_blank',
          //'scrollbars=yes,location=yes,menubar=yes,resizable=yes,status=yes,titlebar=yes,toolbar=yes'
      //);
    }
  });

  $('.send-email-now').bind('click', function(e) {
    e.preventDefault();
    var el = $(this);
    enviar.show({
      id: el.parents('.reveal-modal').attr('id')
    });
  });
});
