$(function() {
  $('.checkoption').checkOption();
  $('.dropdown').dropDown();

  $('.open-tip').bind('click', function(event) {
    event.preventDefault();
    var el = $(this);

    if (el.hasClass('open-tip-open')) {
      el.removeClass('open-tip-open')
        .parents('.module-tip').find('.module-inner-alter').hide();
    }

    else {
      el.addClass('open-tip-open')
        .parents('.module-tip').find('.module-inner-alter').show();
    }
  });

  $('.tip-item').bind('click', function(event) {
    event.preventDefault();
    var el = $(this);
    var parent = el.parents('.module-inner');
    parent.find('.open').removeClass('open');

    if (el.hasClass('open')) {
      el
        .removeClass('open')
      .next()
        .removeClass('open');
    }

    else {
      el
        .addClass('open')
      .next()
        .addClass('open');
    }
  });

  /* PLACEHOLDER FOR FORMS */
  /* Remove this and jquery.placeholder.min.js if you don't need :) */
  $('input, textarea').placeholder();


  $('.printnow').bind('click', function(e) {
    e.preventDefault();
    window.print();
  });

  $('.fx-button').bind('mousedown', function() {
    var el = $(this);
    el.addClass('fx-button-push');
    if (el.attr('class').indexOf('shadow') !== -1) {
      el.addClass('no-shadow');
    }
  }).bind('mouseup', function() {
    var el = $(this);
    el.removeClass('fx-button-push');
    if (el.attr('class').indexOf('shadow') !== -1) {
      el.removeClass('no-shadow');
    }
  }).bind('mouseleave', function() {
    var el = $(this);
    el.removeClass('fx-button-push');
    if (el.attr('class').indexOf('shadow') !== -1) {
      el.removeClass('no-shadow');
    }
  });

  $('#menu .menu-item').bind('mouseenter', function() {
    var el = $(this);
    //el.next('.tooltip').width(el.width() - 20).fadeTo(200, .9);
    el.next('.tooltip').width(el.width() - 10).fadeTo(200, .8);
  }).bind('mouseleave', function() {
    var el = $(this);
    el.next('.tooltip').fadeTo(200, 0);
  });

  $('.open-popup .link').attr('target', '_blank'); 

  //$('.open-popup .link').bind('click', function(e) {
    //e.preventDefault();
    //var el = $(this);
    //window.open(el.attr('href'), 'height=730,width=980,scrollbars=yes,location=yes,menubar=yes,resizable=yes,status=yes,titlebar=yes,toolbar=yes');
  //});
});
