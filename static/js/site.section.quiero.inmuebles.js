$(function() {
  var filters = {

    elements: {},

    optionsBuild: function(combo, data, def) {
      var tpl = combo.html();
      var html = '';
      var index = 0;

      $.each(data, function(i, o) {
        if (typeof o === 'string') {
          // type
          if (index === 0 && def) {
            html += tpl.split(/-text-/).join(def.text)
              .split(/-value-/).join(def.value)
              .split(/-0/).join('-' + def.index);
          }

          html += tpl.split(/-text-/).join(o)
            .split(/-value-/).join(i)
            .split(/-0/).join('-' + index);
        }

        else {
          // departments
          if (index === 0 && def) {
            html += tpl.split(/-text-/).join(def.text)
              .split(/-value-/).join(def.value)
              .split(/-0/).join('-' + def.index);
          }

          html += tpl.split(/-text-/).join(o.name)
            .split(/-value-/).join(o.department_code)
            .split(/-0/).join('-' + index);
        }

        ++index;
      });

      combo.html(html);
    },

    queryString: function(input) {
      var r, o = {};
      if (typeof input === 'string') {
        r = input.substr(1, input.length).split('&');
        for (var i in r) {
          if (typeof r[i] !== 'function') {
            if (r[i]) {
              input = r[i].split('=');
              o[input[0]] = input[1];
            }
          }
        }
      }

      else if (typeof input === 'object') {
        r = [];
        for (var i in input) {
          if (typeof input[i] !== 'function') {
            r.push(i + '=' + input[i]);
          }
        }

        o = '?' + r.join('&');
      }

      return o;
    },

    init: function() {
      var self = this;

      self.elements.form = $('#filter');
      self.elements.dis = $('#departments');
      self.elements.type = $('#types');
      self.elements.specs = $('#specs');
      self.elements.pmin = $('#pmin');
      self.elements.pmax = $('#pmax');

      if (departments) {
        self.optionsBuild(
            self.elements.dis.find('.drop-list'), departments || [], {
              text: 'Todos',
              value: '',
              index: 'def'
            }
        );
      }

      if (types) {
        self.optionsBuild(self.elements.type.find('.drop-list'), types || [], {
          text: 'Todos',
          value: '',
          index: 'def'
        });
      }

      self.elements.form.bind('submit', function(e) {
        e.preventDefault();
        var el = self.elements.form;

        var search = {};
        var dis = self.elements.dis.data('dropData').getValue();
        var type = self.elements.type.data('dropData').getValue();
        var specs = self.elements.specs.data('dropData').getValue();
        var pmin = self.elements.pmin.val();
        var pmax = self.elements.pmax.val();

        if (dis && dis.indexOf('=') === -1 ) {
          search.dis = dis;
        }

        if (type && type.indexOf('=') === -1 ) {
          search.type = type;
        }

        if (specs && specs.indexOf('=') === -1 ) {
          search.specs = specs; 
        }

        if (pmin && pmin !== self.elements.pmin.attr('placeholder')) {
          search.pmin = pmin;
        }

        if (pmax && pmax !== self.elements.pmax.attr('placeholder')) {
          search.pmax = pmax;
        }

        search = self.queryString(search);
        location.search = search;
      });

      var query = self.queryString(location.search);
      if (query.dis) {
        self.elements.dis.data('dropData').setValue(query.dis);
      }

      if (query.type) {
        self.elements.type.data('dropData').setValue(query.type);
      }

      if (query.specs) {
        self.elements.specs.data('dropData').setValue(query.specs);
      }

      if (query.pmin) {
        self.elements.pmin.val(query.pmin);
      }

      if (query.pmax) {
        self.elements.pmax.val(query.pmax);
      }

      return self;
    }

  }.init();

  var paginate = {

    elements: {},

    init: function() {
      var self = this;
      self.elements.perpage = $('.perpage');

      var query = filters.queryString(location.search);
      query.per_page = query.per_page || 10;

      if (query.per_page) {
        self.elements.perpage.each(function() {
          $(this).data('dropData').setValue(query.per_page);
        });
      }

      self.elements.perpage.bind('dropDown:setValue', function() {
        var p = $(this).find(':checked').val();
        if (p) {
          var query = filters.queryString(location.search);
          query.per_page = p;
          delete query.page;
          location.search = filters.queryString(query);
        }
      });

      return self;
    }

  }.init();
});
