$(function() {
  var link = $('#video_link');
  var video = $('#video_content').html();

  function cleanDemo() {
    $('#video_content').html('');
  }

  link.bind('click', function() {
    $('#video_content').html(video);
    $('#demo').bind('reveal:close', cleanDemo);
  });
});
