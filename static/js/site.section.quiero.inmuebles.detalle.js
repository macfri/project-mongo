$(function() {
  var gallery = {

    pixel_step: 94,

    elements: {},

    get: function(query) {
      return $(query, this.elements.gallery);
    },

    show: function(e) {
      e.preventDefault();
      var self = gallery;
      var el = $(this);

      if (el.hasClass('active')) {
        return;
      }

      self.get('.active').removeClass('active');
      el.addClass('active');
      var photo = self.get('#photo_gal');

      self.elements.loading.spin();

      photo.fadeOut(function() {
        photo
          .css({
            visibility: 'hidden',
            display: 'block'
          })
          .attr('src', el.attr('href'))
          .load(function() {
            photo.hide().css('visibility', 'visible').fadeIn();
            $(self.elements.loading.data('spinner').el).fadeOut();
          });
      });
    },

    slider: function(dir) {
      var self = this;
      self.elements.slider.animate({
        top: 0
      });
    },

    up: function(e) {
      e.preventDefault();
      var el = $(this);
      var self = gallery;
      var top = self.elements.slider.position().top;

      if (top < 0) {
        self.elements.slider.animate({
          top: '+=' + self.pixel_step + 'px'
        });
      }

      if (top + self.pixel_step >= 0) {
        el.css('visibility', 'hidden');
      }

      if (self.elements.down.css('visibility') === 'hidden') {
        self.elements.down.css('visibility','visible');
      }
    },

    down: function(e) {
      e.preventDefault();
      var el = $(this);
      var self = gallery;
      var top = Math.abs(self.elements.slider.position().top);
      var height = self.elements.slider_cont.height() + self.pixel_step;

      if (top < height) {
        self.elements.slider.animate({
          top: '-=' + self.pixel_step + 'px'
        });
      }

      if (top + self.pixel_step > height) {
        el.css('visibility', 'hidden');
      } 

      if (self.elements.up.css('visibility') === 'hidden') {
        self.elements.up.css('visibility','visible');
      }
    },

    init: function() {
      var self = this;
      self.elements.gallery = $('#gallery');
      self.elements.loading = $('#gallery_inner');
      self.elements.slider = $('#gallery_nav_slide');
      self.elements.slider_cont = $('#gallery_nav_slide_cont');
      self.elements.up = $('#gallery_nav_up').bind('click', self.up)
        .css('visibility', 'hidden');
      self.elements.down = $('#gallery_nav_down').bind('click', self.down);
      self.elements.gallery_pics = self.get('.pic').bind('click', self.show);


      self.elements.gallery.bind('reveal:open', function() {
        if ((self.elements.slider.children().length * 94) < 
          self.elements.slider.height()) {
          self.elements.down.hide();
        }
      });

      return self;
    }

  }.init();

});
