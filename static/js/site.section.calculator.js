$(function() {
  function charts(config) {
    config = config || {};

    var colors = {
      '100': '#FD5003',
      '85': '#FF6825',
      '70': '#FF834D',
      '55': '#FF9E73',
      '40': '#FFB898',
      '25': '#FFD3C0',
      '10': '#FFEDE6'
    };

    var self = this;
    return self.each(prepare);

    function update(key, percent) {
      var self = this;
      var item = this.items[key];
      var zIndex = 1001;
      var chart = $('.' + item.chart, item.el);
      var label = $('.' + item.label, item.el);
      var chart_css;

      item.percent = parseFloat(percent);

      if (item.percent < 0.1) {
        label.hide();
      }

      else if (item.percent > 100) {
        label.hide();
        item.percent = 100;
      }

      else {
        label.show();
      }

      if (this.type !== 2) {
        var h = chart.css({
          height: (item.percent * this.height) / 100,
          zIndex: Math.ceil(zIndex - item.percent)
        }).height();


        //label.css({
      //top: chart.position().top
        //});
      }

      else {
        chart_css = {
          height: item.percent + '%',
          zIndex: Math.ceil(zIndex - item.percent)
        };
        var h = chart.css(chart_css).height();

        //label.css({
        //bottom: h
        //});
      }

      label.find('.text').html(percent);

      if (this.type !== 2) {
        this.charts_content.height(this.total());
        //this.labels_content.height(this.total());
      }
    }

    function clean() {
      var self = this;
      var label_css;

      $.each(self.items, function(i, item) {
        if (self.type !== 2) {
          label_css = {
            top: 0
          };
        }

        else {
          label_css = {
            bottom: 0
          };
        }

        item.percent = 0;

        $('.' + item.chart, item.el).height(0);

        $('.' + item.label, item.el)
        //.css(label_css)
          .hide().find('.text').html('0');
      });

      if (self.type !== 2) {
        self.charts_content.height(0);
        //self.labels_content.height(0);
      }

      if (self.inputs) {
        self.inputs.each(function() {
          this.value = '0';
        });
      }
    }

    function total() {
      var self = this;
      var t = 0;
      $.each(self.items, function(i, item) {
        t += parseFloat(item.percent * self.height) / 100;
      });

      return t;
    }

    function prepare() {
      var el = $(this);
      var data = {
        el: el,
        charts_content: el.find('.charts-content'),
        items: {},
        total: total,
        update: update,
        clean: clean,
        type: config.type
      };
      data.height = data.charts_content.height();

      el.data('chartData', data);

      if (config.items) {
        var html = '';
        var html_chart = '';
        var html_label = '';
        var html_label_static = '';
        var i = 0;

        $.each(config.items, function(name, o) {

          if (typeof o === 'object' && o.static) {
            html_label_static += '<div class="chart-label label-item-' + name +
                '" style="display: none"><span class="icon-label"></span>' +
                o.label +
                '</div>';
          }

          else {
            html_chart += '<div class="chart chart-' + i + ' chart-item-' + i +
                '"></div>';

            var label = o;

            if (typeof o === 'object' && o.label) {
              label = o.label;
              html_label += '<div class="chart-label label-item-' + i +
                  '" style="display: none"><span class="icon-label"></span>' +
                  label +
                  '</div>';
            }

            else {
              html_label += '<div class="chart-label label-item-' + i +
                  '" style="display: none"><span class="icon-label"></span>' +
                  '<span class="text">0</span>% ' +
                  label +
                  '</div>';
            }

            data.items[name] = {
              el: el,
              chart: 'chart-item-' + i,
              label: 'label-item-' + i,
              percent: 0
            };
            ++i;
          }
        });


        data.charts_content.prepend(html_chart);
        data.el.prepend(
            '<div class="labels-content">' +
            html_label_static + html_label +
            '</div>'
        );
        data.labels_content = el.find('.labels-content');

        if (data.type !== 2) {
          data.charts_content.height(0);
          data.labels_content.height(0).css({
            position: 'absolute',
            bottom: 12
          });
        }
      }
    }
  }

  $.fn.charts = charts;

  window.calculator = {
    els: {},

    oneUSDtoPEN: 2.69,
    onePENtoUSD: 0.37,
    PEN_PREFIX: 'S/. ',
    USD_PREFIX: 'US$ ',
    // dolares
    TASACION: 95,
    // soles
    NOTARIALES: 450,

    TEA: {
      get: function(mount, years) {
        var self = this;
        var items;
        var r;
        var i = 1;

        if (calculator.p3.getTypeMoney() === 'dolares') {
          items = self.dolares;
        }

        else {
          items = self.soles;
        }

        for (o in items) {
          o = items[o];

          if (o.a === '&') {

            if (mount >= o.de) {
              if (years <= 15) {
                r = o.hasta15;
              }
              else {
                r = o.mayor15;
              }

              break;
            }
          }
          else {
            if (mount >= o.de && mount <= o.a) {
              if (years <= 15) {
                r = o.hasta15;
              }
              else {
                r = o.mayor15;
              }

              break;
            }

            else if (i === 1 && mount < items[0].de) {
              if (years <= 15) {
                r = items[0].hasta15;
              }
              else {
                r = items[0].mayor15;
              }
              break;
            }
          }
        }

        return r;
      },

      'dolares': [
        {
          'de': 10000,
          'a': 24999,
          'hasta15': 13.80,
          'mayor15': 14.05
        },

        {
          'de': 25000,
          'a': 39999,
          'hasta15': 11.20,
          'mayor15': 11.50
        },

        {
          'de': 40000,
          'a': 49999,
          'hasta15': 10.10,
          'mayor15': 10.30
        },

        {
          'de': 50000,
          'a': 59999,
          'hasta15': 9.70,
          'mayor15': 9.90
        },

        {
          'de': 60000,
          'a': 79999,
          'hasta15': 9.40,
          'mayor15': 9.70
        },

        {
          'de': 80000,
          'a': 99999,
          'hasta15': 9.10,
          'mayor15': 9.30
        },

        {
          'de': 100000,
          'a': 149999,
          'hasta15': 8.90,
          'mayor15': 9.10
        },

        {
          'de': 150000,
          'a': 199999,
          'hasta15': 8.60,
          'mayor15': 8.90
        },

        {
          'de': 200000,
          'a': 299999,
          'hasta15': 8.50,
          'mayor15': 8.70
        },

        {
          'de': 300000,
          'a': 499999,
          'hasta15': 8.30,
          'mayor15': 8.60
        },

        {
          'de': 500000,
          'a': '&',
          'hasta15': 8.20,
          'mayor15': 8.50
        }
      ],

      'soles': [
        {
          'de': 32000,
          'a': 74999,
          'hasta15': 13.80,
          'mayor15': 14.05
        },
        {
          'de': 75000,
          'a': 119999,
          'hasta15': 11.70,
          'mayor15': 11.70
        },
        {
          'de': 120000,
          'a': 149999,
          'hasta15': 10.50,
          'mayor15': 10.70
        },
        {
          'de': 150000,
          'a': 179999,
          'hasta15': 10.10,
          'mayor15': 10.50
        },
        {
          'de': 180000,
          'a': 239999,
          'hasta15': 9.40,
          'mayor15': 9.80
        },
        {
          'de': 240000,
          'a': 299999,
          'hasta15': 9.20,
          'mayor15': 9.50
        },
        {
          'de': 300000,
          'a': 449999,
          'hasta15': 9.00,
          'mayor15': 9.30
        },
        {
          'de': 450000,
          'a': 599999,
          'hasta15': 8.70,
          'mayor15': 9.00
        },
        {
          'de': 600000,
          'a': 899999,
          'hasta15': 8.60,
          'mayor15': 8.90
        },
        {
          'de': 900000,
          'a': 1499999,
          'hasta15': 8.40,
          'mayor15': 8.80
        },
        {
          'de': 1500000,
          'a': '&',
          'hasta15': 8.30,
          'mayor15': 8.70
        }
      ]
    },

    changeTab: function(el, dir) {
      var self = this;
      var current_content = self.els.contents.filter(':visible');
      var current_api = current_content.data('calculator');
      var next_content = $('#' + el.attr('data-tab-id'));
      var perdonad = false;

      if (
          el.hasClass('block-step') === false &&
          parseInt(current_content.attr('id').split('p').join(''), 10) >
          parseInt(next_content.attr('id').split('p').join(''), 10)
      )
      {
        perdonad = true;
      }


      if (
          perdonad === false &&
          current_api &&
          typeof current_api.validate === 'function' &&
          current_api.validate() === false
      ) {
        //self.els.next.addClass('inactive');
        return;
      }
      //else {
      //self.els.next.removeClass('inactive');
      //}

      if (perdonad === true) {
        self.els.next.removeClass('inactive');
      }

      else {
        self.els.next.addClass('inactive');
      }

      /* desactivo el tab y contenido actual */
      self.els.tabs.filter('.active').removeClass('active');
      current_content.hide();

      /* Activo contenido siguiente */
      next_content.show()
        .trigger('calculator:show', { tab: el });
      el.addClass('active');
      el.removeClass('block-step');
      //el.removeClass('notview-step');
      $('.cal-step:lt(' + (el.index() + 1) + ')').removeClass('notview-step');

      /* oculto controles de navegacion entre tabs al llegar a los extremos del
       * contenido */
      if (el.index() > 0) {
        self.els.prev.show();
      }

      else {
        self.els.prev.hide();
      }

      if (el.index() > 9) {
        self.els.next.hide();
      }

      else {
        self.els.next.show();
      }
    },

    tabChangeTo: function(e) {
      e.preventDefault();
      var el = $(this);
      if (el.hasClass('block-step')) {
        return;
      }
      calculator.changeTab(el);
    },

    tabNext: function(e) {
      e.preventDefault();
      var self = calculator;
      var current = self.els.contents.filter(':visible');
      var current_data = current.data('calculator');
      var next = self.els.tabs.filter('.active').next();

      if (next.length === 0) {
        next = self.els.tabs.filter(':first');
      }

      if (current_data && current_data.next) {
        next = current_data.next;
      }

      calculator.changeTab(next);
    },

    tabPrev: function(e) {
      e.preventDefault();
      var self = calculator;
      var current = self.els.contents.filter(':visible');
      var current_data = current.data('calculator');
      var next = self.els.tabs.filter('.active').prev();

      if (next.length === 0) {
        next = self.els.tabs.filter(':last');
      }

      if (current_data && current_data.prev) {
        next = current_data.prev;
      }

      calculator.changeTab(next);
    },

    p1: {
      is_init: 0,
      els: {},
      validate: function() {
        var self = this;
        var status = 0;
        self.els.errors.hide();

        if (self.els.radios.filter(':checked').length === 0) {
          status |= 1;
        }

        if (status === 0) {
          calculator.els.next.removeClass('inactive');
          return true;
        }
        else {
          calculator.els.next.addClass('inactive');
          $('#error11').show();
          return false;
        }
      },
      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p1');
        self.els.radios = $(':radio', self.els.el);
        self.els.errors = $('.error', self.els.el);
        self.els.el.data('calculator', self);

        self.els.radios.bind('change', function() {
          self.validate();

          if (parseInt(this.value, 10) === 1) {
            self.next = $('#tabstep2');
            $('#tabstep3').addClass('block-step notview-step');
            calculator.p3.prev = $('#tabstep2');
          }
          else {
            $('#tabstep2').addClass('block-step notview-step');
            self.next = $('#tabstep3');
            calculator.p3.prev = $('#tabstep1');

            //switch(this.value) {
            //case '2':
            //calculator.player.sound('2-2.mp3');
            //break;
            //case '3':
            //calculator.player.sound('2-3.mp3');
            //break;
            //case '4':
            //calculator.player.sound('2-4.mp3');
            //break;
            //}
          }
          
          setTimeout(function() {
            calculator.els.next.trigger('click');
          }, 250);
        });

        calculator.player.sound('1.mp3');

        self.els.el.bind('calculator:show', function() {
          calculator.player.sound('1.mp3');
        });
      }
    },

    p2: {
      is_init: 0,
      els: {},
      validate: function() {
        var self = this;
        var status = 0;
        self.els.errors.hide();


        if (self.els.radios.filter(':checked').length === 0) {
          status |= 1;
        }

        if (status === 0) {
          calculator.els.next.removeClass('inactive');
          return true;
        }
        else {
          calculator.els.next.addClass('inactive');
          $('#error21').show();
          return false;
        }
      },
      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p2');
        self.els.radios = $(':radio', self.els.el);
        self.els.errors = $('.error', self.els.el);
        self.els.el.data('calculator', self);
        self.els.radios.bind('change', function() {
          self.validate();

          switch (this.value) {
            case '1':
              calculator.player.sound('2-1-1.mp3');
              break;
            case '2':
              calculator.player.sound('2-1-2.mp3');
              break;
            case '3':
              calculator.player.sound('2-1-3.mp3');
              break;
            case '4':
              calculator.player.sound('2-1-4.mp3');
              break;
          }

          setTimeout(function() {
            calculator.els.next.trigger('click');
          }, 250);
        });

        calculator.player.sound('2-1.mp3');

        self.els.el.bind('calculator:show', function() {
          calculator.player.sound('2-1.mp3');
        });
      }
    },

    p3: {
      is_init: 0,
      els: {},
      validate: function() {
        var self = this;
        var status = 0;
        self.els.errors.hide();


        if (self.els.radios.filter(':checked').length === 0) {
          status |= 1;
        }

        if (status === 0) {
          calculator.els.next.removeClass('inactive');
          return true;
        }
        else {
          calculator.els.next.addClass('inactive');
          $('#error31').show();
          return false;
        }
      },
      getTypeMoney: function() {
        var self = this;
        var value = parseInt(self.els.radios.filter(':checked').val(), 10);
        if (value === 1) {
          return 'soles';
        }
        else {
          return 'dolares';
        }
      },
      getMoneyPrefix: function() {
        var self = this;
        if (self.getTypeMoney() === 'soles') {
          return calculator.PEN_PREFIX;
        }
        else {
          return calculator.USD_PREFIX;
        }
      },
      convertAccordingType: function(n) {
        var self = this;

        var salary_type = calculator.p5.getTypeMoney();
        var hipoteca_type = calculator.p3.getTypeMoney();
        if (
            salary_type === 'soles' &&
            hipoteca_type === 'dolares'
        )
        {
          return parseInt(calculator.toUSD(parseFloat(n)).toFixed(1), 10);
        }

        else if (
            salary_type === 'dolares' &&
            hipoteca_type === 'soles'
        )
        {
          return parseInt(calculator.toPEN(parseFloat(n)).toFixed(1), 10);
        }

        else {
          return parseInt(parseFloat(n).toFixed(1), 10);
        }
      },

      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p3');
        self.els.radios = $(':radio', self.els.el);
        self.els.errors = $('.error', self.els.el);
        self.els.el.data('calculator', self);

        self.els.radios.bind('change', function() {
          self.validate();

          if(self.els.radios.filter(':checked').val() === '2') {
            $('#p10, #p11').removeClass('soles').addClass('dolares');
          }
          else if (self.els.radios.filter(':checked').val() === '2') {
            $('#p10, #p11').removeClass('dolares').addClass('soles');
          }

          setTimeout(function() {
            calculator.els.next.trigger('click');
          }, 250);
        });

        calculator.player.sound('3.mp3');

        self.els.el.bind('calculator:show', function() {
          calculator.player.sound('3.mp3');
        });
      }
    },

    p4: {
      is_init: 0,
      els: {},
      validate: function() {
        var self = this;
        var status = 0;
        self.els.errors.hide();


        if (self.els.radios.filter(':checked').length === 0) {
          status |= 1;
        }

        if (status === 0) {
          calculator.els.next.removeClass('inactive');
          return true;
        }
        else {
          calculator.els.next.addClass('inactive');
          $('#error41').show();
          return false;
        }
      },
      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p4');
        self.els.radios = $(':radio', self.els.el);
        self.els.errors = $('.error', self.els.el);
        self.els.el.data('calculator', self);

        self.els.radios.bind('change', function() {
          self.validate();
          var el = $(this);
          if (parseInt(el.val(), 10) === 1) {
            calculator.els.el.removeClass('single').addClass('pair');
            $('.pair').show();
            $('.single').hide();
          }
          else {
            calculator.els.el.removeClass('pair').addClass('single');
            $('.pair').hide();
            $('.single').show();
          }

          setTimeout(function() {
            calculator.els.next.trigger('click');
          }, 250);
        });

        calculator.player.sound('4.mp3');

        self.els.el.bind('calculator:show', function() {
          calculator.player.sound('4.mp3');
        });
      }
    },

    p5: {
      is_init: 0,
      els: {},
      validate: function() {
        var self = this;
        var status = 0;
        self.els.errors.hide();


        if (
            calculator.val(self.els.input) === 0 ||
            self.els.input.val() === ''
        ) {
          status |= 1;
        }

        if (status === 0) {
          calculator.els.next.removeClass('inactive');
          return true;
        }
        else {
          calculator.els.next.addClass('inactive');
          $('#error51').show();
          return false;
        }
      },

      getTypeMoney: function() {
        var self = this;
        if (self.els.input2.is(':checked')) {
          return 'soles';
        }
        else {
          return 'dolares';
        }
      },

      getMoneyPrefix: function() {
        var self = this;
        if (self.getTypeMoney() === 'soles') {
          return calculator.PEN_PREFIX;
        }
        else {
          return calculator.USD_PREFIX;
        }
      },

      getGrossSalary: function() {
        var self = this;
        return calculator.val(self.els.input);
      },

      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p5');
        self.els.input = $('#i51');
        self.els.input2 = $('#i52');
        self.els.input3 = $('#i53');
        self.els.text1 = $('#text51').html(
            calculator.PEN_PREFIX + calculator.oneUSDtoPEN
            );
        self.els.errors = $('.error', self.els.el);
        self.els.el.data('calculator', self);

        if (calculator.p3.getTypeMoney() === 'soles') {
          calculator.els.el.removeClass('dolares').addClass('soles');
          calculator.moneda_prefix = calculator.PEN_PREFIX;
          self.els.input2.trigger('click');
        }

        else {
          calculator.els.el.removeClass('soles').addClass('dolares');
          calculator.moneda_prefix = calculator.USD_PREFIX;
          self.els.input3.trigger('click');
        }

        // soles
        self.els.input2.bind('change', function() {
          calculator.els.el.removeClass('dolares').addClass('soles');
          calculator.moneda_prefix = calculator.PEN_PREFIX;
          calculator.chart3Data.setGrossSalary();
        });

        // dolares
        self.els.input3.bind('change', function() {
          calculator.els.el.removeClass('soles').addClass('dolares');
          calculator.moneda_prefix = calculator.USD_PREFIX;
          calculator.chart3Data.setGrossSalary();
        });

        self.els.input.bind('change keyup', function() {
          self.validate();
          calculator.sueldo_bruto = self.getGrossSalary();

          calculator.impuestos = (calculator.sueldo_bruto * 30) / 100;

          calculator.chart1Data.inputsUpdateAll();


          calculator.sueldo_libre = calculator.sueldo_bruto -
              calculator.impuestos;

          calculator.chart3Data.setGrossSalary();

          if (calculator.p8.is_init) {
            calculator.p8.gastosCreditos();
          }

          else if (calculator.p7.is_init) {
            calculator.p7.ahorroMensual();
            calculator.chart1Data.inputsUpdateAll();
          }

          if (
              calculator.p6.is_init &&
              parseInt(calculator.p6.els.radios.filter(':checked').val()) === 1
          )
          {
            calculator.p6.ahorro();
            calculator.p6.credito();
          }
        });

        calculator.player.sound('5.mp3');

        self.els.el.bind('calculator:show', function() {
          calculator.player.sound('5.mp3');
        });
      }
    },

    p6: {
      is_init: 0,
      els: {},
      validate: function() {
        var self = this;
        var checked = self.els.radios.filter(':checked');
        var status = 0;
        self.els.errors.hide();

        /* 1, seleccione una opcion
         * 2, los campos ahorro mensual y destinar credito son requeridos */

        if (checked.length === 0) {
          status |= 1;
        }

        if (
            parseInt(checked.val(), 10) === 1 &&
            (
            calculator.val(self.els.input1) === 0 ||
            self.els.input1.val() === '' ||
            calculator.val(self.els.input2) === 0 ||
            self.els.input2.val() === ''
            )
        ) {
          status |= 2;
        }

        if (status === 0) {
          calculator.els.next.removeClass('inactive');
          return true;
        }
        else {
          calculator.els.next.addClass('inactive');

          if ((status & 2) === 2) {
            $('#error62').show();
          }

          if ((status & 1) === 1) {
            $('#error61').show();
          }
          return false;
        }
      },
      ahorro: function() {
        var self = this;
        var value = calculator.val(self.els.input1);
        var limit = calculator.p5.getGrossSalary();
        var credito = calculator.val(self.els.input2);

        if (value > limit) {
          self.els.input1.val(calculator.addCommas(limit));
          value = limit;
        }

        self.validate();

        var percent = value * 100 /
            limit;
        credito = credito * 100 / limit;

        calculator.chart3Data.update(
            'ahorro',
            percent.toFixed(1)
        );

        calculator.chart3Data.el.find('.label-item-0 .text').html(
            (percent - credito).toFixed(1)
        );

        calculator.ahorro_mensual = value;
      },
      credito: function() {
        var self = this;
        var value = calculator.val(self.els.input2);
        var limit = calculator.val(self.els.input1);

        if (value > limit) {
          self.els.input2.val(calculator.addCommas(limit));
          value = limit;
        }

        self.validate();

        var percent = value * 100 / calculator.sueldo_bruto;

        var ahorro = limit * 100 / calculator.sueldo_bruto;

        calculator.chart3Data.update(
            'destinado_a_credito',
            percent.toFixed(1)
        );

        calculator.chart3Data.el.find('.label-item-0 .text').html(
            (ahorro - percent).toFixed(1)
        );
        

        calculator.destinado_a_credito = value;
      },
      reset1: function() {
        var self = this;
        self.els.group1.show();
        self.els.group2.hide();

        /* bloqueo los tabs siguientes */
        $('#tabstep7').addClass('block-step notview-step');
        $('#tabstep8').addClass('block-step notview-step');
        $('#tabstep9').addClass('block-step notview-step');
        $('#tabstep10').addClass('block-step notview-step');
        $('#tabstep11').addClass('block-step notview-step');

        /* reseteo informacion */
        calculator.chart1Data.clean();
        calculator.ahorro_mensual = 0;

        /* corrijo navegacion */
        self.next = $('#tabstep9');
        calculator.p9.prev = $('#tabstep6');
      },
      reset2: function() {
        /* bloqueo los tabs siguientes */
        var self = this;
        $('#tabstep7').addClass('block-step notview-step');
        $('#tabstep8').addClass('block-step notview-step');
        $('#tabstep9').addClass('block-step notview-step');
        $('#tabstep10').addClass('block-step notview-step');
        $('#tabstep11').addClass('block-step notview-step');
        self.els.group1.hide();
        self.els.group2.show();
        self.els.input1.val('0').trigger('change').blur();
        self.els.input2.val('0').trigger('change').blur();


        calculator.chart3Data.clean();
        calculator.ahorro_mensual = 0;
        calculator.destinado_a_credito = 0;

        self.next = $('#tabstep7');
        calculator.p9.prev = $('#tabstep8');
      },
      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p6');
        self.els.group1 = $('#group61');
        self.els.group2 = $('#group62');
        self.els.input1 = $('#i63');
        self.els.input2 = $('#i64');
        self.els.errors = $('.error', self.els.el);
        self.next = $('#tabstep7');

        self.els.radios = $('#i61, #i62').bind('change', function() {
          $('#i91').val('');

          /* opcion Si, se cuanto ahorro y cuanto quiero endeudarme */
          if (parseInt(this.value, 10) === 1) {
            self.reset1();
          }

          /* opcion no tengo idea, ayudame */
          else {
            self.reset2();

            setTimeout(function() {
              calculator.els.next.trigger('click');
            }, 250);
          }

          //self.validate();
        });

        self.els.input1.bind('keyup change', function() {
          //var value = calculator.val(self.els.input1);
          self.ahorro();

          if (
            $('#tabstep10').hasClass('block-step') === false ||
            $('#tabstep9').hasClass('block-step') === false
          ) {
            self.reset1();
          }
          //if (calculator.val(calculator.p5.els.input) > value) {
          //return false;
          //}
        });

        self.els.input2.bind('keyup change', function() {
          self.credito();
          if (
            $('#tabstep10').hasClass('block-step') === false ||
            $('#tabstep9').hasClass('block-step') === false
          ) {
            self.reset1();
          }
        });

        self.els.el.data('calculator', self);

        calculator.player.sound('6.mp3');

        self.els.el.bind('calculator:show', function() {
          calculator.player.sound('6.mp3');
        });
      }
    },

    p7: {
      is_init: 0,
      els: {},
      sueldo_bruto: 0,
      ahorroMensual: function() {
        var self = this;
        var total = calculator.sueldo_libre;

        self.els.inputs.each(function() {
          total -= calculator.val(this) || 0;
        });

        calculator.ahorro_mensual = total;

        self.els.text2.html(
            calculator.moneda_prefix + calculator.addCommas(total.toFixed(1))
        );

        return total;
      },

      validate: function() {
        var self = this;
        var status = 0;
        self.els.error1.hide();
        //$('#error81').hide();

        if (
            calculator.ahorro_mensual < 0
        )
        {
          status |= 1;
        }

        if (status === 0) {
          //calculator.els.next.removeClass('inactive');
          return true;
        }
        else {
          //calculator.els.next.addClass('inactive');
          self.els.error1.show();
          //$('#error81').show();
          return false;
        }
      },

      init: function() {
        var self = this;
        self.is_init = 1;

        self.els.el = $('#p7');
        self.els.text1 = $('#text71');
        self.els.text2 = $('#text72');
        self.els.text3 = $('#text73');
        self.els.error1 = $('#error71');
        self.els.inputs = $('#i71, #i72, #i73, #i74, #i75')
          .bind('keyup change', function(e) {
              $('#i91').val('');

              self.ahorroMensual();

              if (calculator.p8.is_init) {
                calculator.p8.gastosCreditos();

                /* bloqueo los tabs siguientes */
                $('#tabstep8').addClass('block-step notview-step');
                $('#tabstep9').addClass('block-step notview-step');
                $('#tabstep10').addClass('block-step notview-step');
                $('#tabstep11').addClass('block-step notview-step');
              }

              self.validate();
            });

        self.els.el.data('calculator', self);

        calculator.player.sound('7.mp3');

        self.els.el.bind('calculator:show', function() {
          calculator.player.sound('7.mp3');
        });

      }
    },

    p8: {
      is_init: 0,
      els: {},

      calcularCredito: function(factor) {
        var self = this;
        factor = factor || 1.4;
        return (
            (calculator.val(self.els.input1) * factor) +
            calculator.val(self.els.input2)
          ) / calculator.p5.getGrossSalary();
      },

      gastosCreditos: function() {
        var self = this;
        var sueldo_bruto = calculator.p5.getGrossSalary();
        var input1 = calculator.val(self.els.input1);
        var factor;
        var credito;

        if (
            self.els.input1 !== '' &&
            self.els.input2 !== ''
        )
        {

          if (input1 > (2 * sueldo_bruto)) {
            factor = 4.2;
          }

          else if (input1 > sueldo_bruto && input1 < (2 * sueldo_bruto)) {
            factor = 2.8;
          }

          credito = self.calcularCredito(factor);
          calculator.chart1Data.update('i82',
              credito.toFixed(1)
          );
        }
      },

      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p8');
        self.els.input1 = $('#i81');
        self.els.input2 = $('#i82');
        self.input2_old_value;

        self.els.input1.bind('keyup change', function() {
          self.gastosCreditos();
        });

        self.els.input2.bind('keyup change', function() {
          self.gastosCreditos();
        });

        calculator.player.sound('8.mp3');

        self.els.el.bind('calculator:show', function() {
          calculator.player.sound('8.mp3');
        });
      }
    },

    p9: {
      is_init: 0,
      els: {},
      updateCapacidadAhorro: function() {
        var self = this;

        calculator
          .destinado_a_credito = calculator.val(self.els.input) || 0;

        var percent = (calculator.val(self.els.input) * 100) /
            calculator.sueldo_bruto;

        self.percent = percent;

        if (
            parseInt(calculator.p6.els.radios.filter(':checked').val(), 10) ===
            1
        ) {
          calculator.chart3Data.update(
              'destinado_a_credito', percent.toFixed(1)
          );
        }

        else {
          calculator.chart1Data.update(
              'credito',
              percent.toFixed(1)
          );

          calculator.chart1Data.inputsUpdateAll();
        }

        self.els.text4.html(percent.toFixed(1));
      },

      validate: function() {
        var self = this;
        var status = 0;
        self.els.errors.hide();

        if (self.percent > 30) {
          status = 1;
        }

        if (self.percent < 0) {
          status = 2;
        }

        if (status === 0) {
          calculator.els.next.removeClass('inactive');
          self.els.group4.show();
          return true;
        }
        else if (status === 1) {
          calculator.els.next.addClass('inactive');
          $('#error91').show();
          self.els.group4.hide();
          return false;
        }

        else if (status === 2) {
          calculator.els.next.addClass('inactive');
          self.els.group4.hide();

          self.els.group6.show();
          self.els.group7.hide();

          return false;
        }
      },

      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p9');
        self.els.text1 = $('.text91');
        self.els.text2 = $('.text92');
        self.els.text3 = $('.text93');
        self.els.text4 = $('#text94');
        self.els.text5 = $('.text95');
        self.els.text6 = $('.text96');
        self.els.input = $('#i91');
        self.els.group1 = $('#group91');
        self.els.group2 = $('.group92');
        self.els.group3 = $('.group93');
        self.els.group4 = $('#group94');
        self.els.group5 = $('.group95');
        self.els.group6 = $('#group96');
        self.els.group7 = $('#group97');
        self.els.error1 = $('#error91');
        self.els.errors = $('.error', self.els.el);
        self.els.el.data('calculator', self);
        self.percent;

        self.els.input.bind('keyup', function() {
          self.updateCapacidadAhorro();
          self.validate();

          if (calculator.p6.els.radios.filter(':checked').val() === '1') {
            calculator.p6.els.input2.val(this.value);
            calculator.p6.credito();
          }
        });
      }
    },

    p10: {
      is_init: 0,
      els: {},
      validate: function() {
        var self = this;
        var status = 0;
        self.els.errors.hide();

        if (
            self.els.input1.val() === '' ||
            calculator.val(self.els.input1) === 0 ||
            self.els.input2.val() === '' ||
            calculator.val(self.els.input2) === 0
        )
        {
          status |= 1;
        }


        if (status === 0) {
          calculator.els.next.removeClass('inactive');
          return true;
        }
        else {
          calculator.els.next.addClass('inactive');
          $('#error101').show();
          return false;
        }
      },

      setChartValues: function() {
        var self = this;

        self.validate();

        if (self.els.input1.val() !== '') {
          calculator.valor_casa = calculator.val(self.els.input1);
          calculator.inicial_casa = calculator.val(self.els.input2);

          var percent = parseInt(calculator.inicial_casa, 10) * 100 /
              calculator.valor_casa;

          calculator.chart2Data.update(
              'cuota_inicial',
              percent.toFixed(1)
          );

          calculator.chart2Data.el.find('#cuota_inicial').html(
              calculator.p3.getMoneyPrefix() +
              calculator.addCommas(calculator.inicial_casa.toFixed(1))
          );

          var cierre_percent = 0;

          if (calculator.gastos_cierre > 0 && calculator.valor_casa > 0) {
            cierre_percent = (
                calculator.gastos_cierre * 100 / calculator.valor_casa
                );

            if (cierre_percent < 1) {
              cierre_percent = 3;
            }
          }

          calculator.chart2Data.update(
              'cierre',
              cierre_percent
          );

          if (calculator.valor_casa > 0) {
            calculator.chart2Data.el.find('.label-item-valor_casa').show()
              .find('.text').html(
                calculator.p3.getMoneyPrefix() +
                calculator.addCommas(
                calculator.valor_casa.toFixed(1)
                ) + ' '
                );
          }
          else {
            calculator.chart2Data.el.find('.label-item-valor_casa').hide();
          }

          $('#charttext').html(
              calculator.p3.getMoneyPrefix() +
              calculator.addCommas(calculator.gastos_cierre.toFixed(1))
          );
        }
      },

      gastosRegistrales: function() {
        var self = this;
        var gastos_cierre;
        var gastos_registrales = calculator.val(calculator.p10.els.input1) *
            2.82 /
            1000;

        gastos_registrales = calculator.toPEN(gastos_registrales);

        if (calculator.moneda_prefix === calculator.USD_PREFIX) {
          gastos_registrales = calculator.toUSD(gastos_registrales);
        }

        // Credito en soles
        if (calculator.p3.getTypeMoney() === 'soles') {
          self.els.el.addClass('soles').removeClass('dolares');
          self.els.text1.html(
              calculator.PEN_PREFIX +
              calculator.toPEN(calculator.TASACION) + ' '
          );

          self.els.text2.html(
              calculator.PEN_PREFIX +
              calculator.NOTARIALES + ' '
          );

          gastos_cierre = calculator.toPEN(calculator.TASACION) +
              calculator.NOTARIALES;
        }

        // credito en dolares
        else {
          self.els.el.addClass('dolares').removeClass('soles');
          self.els.text1.html(
              calculator.USD_PREFIX +
              calculator.TASACION + ' '
          );

          self.els.text2.html(
              calculator.USD_PREFIX +
              calculator.toUSD(calculator.NOTARIALES) + ' '
          );

          gastos_cierre = calculator.toUSD(calculator.NOTARIALES) +
              calculator.TASACION;
        }



        //self.els.text3.html(
        //calculator.moneda_prefix +
        //calculator.addCommas(gastos_registrales)
        //);

        calculator.gastos_cierre = gastos_cierre;
        calculator.gastos_registrales = gastos_registrales;
        self.setChartValues();
      },

      cuotaInicial: function() {
        var self = this;
        var value = calculator.val(self.els.input2);
        var limit = calculator.val(self.els.input1);

        if (value > limit) {
          self.els.input2.val(
              calculator.addCommas(limit)
          );
        }

        self.gastosRegistrales();

        var gastos_operacion = calculator.gastos_cierre;
        var cuota_inicial = calculator.val(calculator.p10.els.input2) -
            gastos_operacion;

        calculator.cuota_inicial = cuota_inicial;
        self.setChartValues();
      },

      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p10');
        self.els.input1 = $('#i101');
        self.els.input2 = $('#i102');
        self.els.text1 = $('#text101');
        self.els.text2 = $('#text102');
        self.els.text3 = $('#text103');
        self.els.errors = $('.error', self.els.el);
        self.els.el.data('calculator', self);

        self.els.input2.bind('keyup change', function() {
          self.cuotaInicial();
        });

        self.els.input1.bind('keyup change', function() {
          var limit = calculator.val(self.els.input1) * 0.1;
          var inicial = calculator.val(self.els.input2);

          if (inicial < limit) {
            self.els.input2.val(
                calculator.addCommas(limit.toFixed(0))
            );
          }
          self.cuotaInicial();
        });
      }
    },

    p11: {
      is_init: 0,
      els: {},
      is_success: 0,
      is_error: 0,
      cuotaMensual: function(years, monto_financiar) {
        monto_financiar = monto_financiar || calculator.monto_financiar;

        var TEA = calculator.TEA.get(
            monto_financiar, years
            ) / 100;

        var meses = years * 12;

        // =(POTENCIA((1+G3),(1/12))-1)*(365/360)+0.049%
        var interes_mensual = (Math.pow(1 + TEA, (1 / 12)) - 1) *
            (365 / 360) + 0.049 / 100;
        calculator.interes_mensual = interes_mensual;

        //=(monto_financiar*interes_mensual)/(1-POTENCIA((1+interes_mensual),
        //(meses)))
        var cuota_efectiva = (monto_financiar * interes_mensual) /
            (1 - Math.pow((1 + interes_mensual), -meses));

        var portes = 3.50;
        if (calculator.p3.getTypeMoney() === 'soles') {
          portes = calculator.toPEN(portes);
        }

        var valor_inmueble = monto_financiar / 0.8;

        // =0.029/100*(valor_inmueble*0.9)
        var seguro_inmueble = 0.029 / 100 *
            (valor_inmueble * 0.9);


        var cuota_hipotecaria = cuota_efectiva + portes + seguro_inmueble;

        /*
        console.log(
          'moneda sueldo %s, moneda hipoteca %s, TEA %s, meses %s,
          interes_mensual %s, cuota_efectiva %s, ' +
          'portes %s, valor_inmueble %s, seguro_inmueble %s, ' +
          'cuota_hipotecaria %s',
          calculator.p5.getTypeMoney(), calculator.p3.getTypeMoney(),TEA,
          meses, interes_mensual, cuota_efectiva, portes, valor_inmueble,
          seguro_inmueble, cuota_hipotecaria
        );
        /**/

        return cuota_hipotecaria;
      },

      displayCuotaMensual: function(years, monto_financiar) {
        var self = this;

        years = years || 15;
        self.anios_default = years;

        monto_financiar = monto_financiar || calculator.monto_financiar;

        var cuota_number = self.cuotaMensual(years, monto_financiar)
          .toFixed(1);

        var cuota = calculator.addCommas(cuota_number.toString());


        /* destinado a credito */
        self.els.text1.html(
            //calculator.p3.getMoneyPrefix() +
            calculator.addCommas(
            calculator.p3.convertAccordingType(
            calculator.destinado_a_credito
            ).toFixed(1)
            )
        );

        /* cuota mensual */
        self.els.text4.html(cuota);
        self.els.text3.html(cuota);

        /* anios */
        self.els.text6.html(years + '');
        self.els.text5.html(years + '');


        self.els.text2.html(
            calculator.addCommas(monto_financiar)
        );


        /* felicitaciones */
        if (
            cuota_number <
            calculator.p3.convertAccordingType(
            calculator.destinado_a_credito
            )
        ) {
          self.els.group2.show();
          self.els.group1.hide();
          self.is_error = 0;
          if (self.is_success === 0) {
            calculator.player.sound('12.mp3');
            self.is_success = 1;
          }
        }

        /* mensaje de advertencia */
        else {
          self.els.group2.hide();
          self.els.group1.show();
          self.is_success = 0;
          //calculator.player.stop();
          if (self.is_error === 0) {
            calculator.player.sound('11.mp3');
            self.is_error = 1;
          }
        }

        //calculator.TEA.get(self.monto_financiar, 4);

        calculator.els.legal.show();
      },

      init: function() {
        var self = this;
        self.is_init = 1;
        self.els.el = $('#p11');
        self.els.input = $('#i111');
        self.els.text1 = $('.text111');
        self.els.text2 = $('.text112');
        self.els.text3 = $('.text113');
        self.els.text4 = $('.text114');
        self.els.text5 = $('.text115');
        self.els.text6 = $('.text116');
        self.els.text7 = $('#text117');
        self.els.group1 = $('#group111');
        self.els.group2 = $('#group112');

        self.els.slider = $('#slider').slider({
          min: 4,
          max: 25,
          value: 15,
          slide: function(event, ui) {
            self.displayCuotaMensual(ui.value);
          }
        });

        self.els.input.bind('change keyup', function(e) {
          var monto_financiar = calculator.val(self.els.input) || 0;
          calculator.monto_financiar = monto_financiar;

          // 90% del valor del inmueble
          //var limit = 90 * calculator.val(calculator.p10.els.input1) / 100;
          var limit = calculator.val(calculator.p10.els.input1);
          //calculator.cuota_inicial;

          var nueva_cuota_inicial = calculator.val(
              calculator.p10.els.input1
              ) +
              calculator.gastos_cierre -
              monto_financiar;

          if (
              e.type === 'keyup' &&
              Math.abs(
              calculator.val(calculator.p10.els.input2) - nueva_cuota_inicial
              ) > 0.9
          ) {
            calculator.cuota_inicial = nueva_cuota_inicial -
                calculator.gastos_cierre;

            calculator.p10.els.input2.val(
                calculator.addCommas(calculator.cuota_inicial)
            );

            calculator.p10.setChartValues();

            if (
                monto_financiar >
                limit
            )
            {
              monto_financiar = limit;
              self.els.input.val(
                  calculator.addCommas(limit)
              );
            }

            self.displayCuotaMensual(
                self.els.slider.slider('value'),
                monto_financiar
            );
          }

        });

        self.els.el.data('calculator', self);

        self.anios_default = 15;
        self.displayCuotaMensual(15);
      }
    },

    val: function(el) {
      if (el.length) {
        el = el[0];
      }
      return parseInt(el.value.split(',').join(''), 10) || 0;
    },

    toPEN: function(n) {
      var self = this;
      return parseFloat((n * self.oneUSDtoPEN).toFixed(1));
    },

    toUSD: function(n) {
      var self = this;
      return parseFloat((n * self.onePENtoUSD).toFixed(1));
    },

    addCommas: function(nStr) {
      nStr += '';
      var x = nStr.split('.');
      var x1 = x[0];
      var x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;

      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
    },

    player: {
      api: 0,

      id: 'audio_player',

      e: {},

      is_pausa: false,
      is_mute: false,

      sound: function(pista) {
        var self = this;
        if (!self.api) {
          self.api = document.getElementById(self.id);
        }

        self.api.stopAllSounds();

        self.api.playSound(pista);

        if (self.is_pausa) {
          self.pausa();
        }

        if (self.is_mute) {
          self.mute();
        }
        self.current_pista = pista;
      },

      stop: function() {
        var self = this;
        if (!self.api) {
          self.api = document.getElementById(self.id);
        }
        self.api.stopAllSounds();
      },

      play: function() {
        var self = this;
        if (!self.api) {
          self.api = document.getElementById(self.id);
        }
        self.is_pausa = false;
        self.api.resume();
      },

      pausa: function() {
        var self = this;
        if (!self.api) {
          self.api = document.getElementById(self.id);
        }
        self.is_pausa = true;
        self.api.pause();
      },

      mute: function() {
        var self = this;
        if (!self.api) {
          self.api = document.getElementById(self.id);
        }
        self.is_mute = true;
        self.api.mute();
      },

      volume: function() {
        var self = this;
        if (!self.api) {
          self.api = document.getElementById(self.id);
        }
        self.is_mute = false;
        self.api.unmute();
      },

      init: function() {
        var self = this;
        self.e.btn_play_stop = $('#audio_play').bind('click', function(e) {
          e.preventDefault();

          if (self.e.btn_play_stop.hasClass('pausa')) {
            self.e.btn_play_stop.removeClass('pausa');
            self.pausa();
          }

          else {
            self.e.btn_play_stop.addClass('pausa');
            self.play();
          }
        });

        self.e.btn_stop = $('#audio_stop').bind('click', function(e) {
          e.preventDefault();

          if (self.e.btn_stop.hasClass('mute')) {
            self.e.btn_stop.removeClass('mute');
            self.volume();
          }

          else {
            self.e.btn_stop.addClass('mute');
            self.mute();
          }
        });

        swfobject.embedSWF(
            '/static/swf/sounds.swf',
            self.id,
            '1',
            '1',
            '9.0.0',
            '/static/swf/expressInstall.swf',
            /* flashvars */
            'false',
            /* params */
            {
              menu: 'false',
              allowscriptaccess: 'always',
              wmode: 'opaque'
            },
            /* attributes */
            {},
            function() {
              self.api = document.getElementById(self.id);
              setTimeout(function() {
                self.sound('0.mp3');
              }, 1000);
            }
        );
        return self;
      }
    },

    init: function() {
      var self = this;
      self.els.el = $('#content');
      self.els.aviso = $('#aviso');
      self.els.tabs = $('.cal-step').bind('click', self.tabChangeTo);
      self.els.contents = $('.cal-content');
      self.els.next = $('#next').bind('click', self.tabNext);
      self.els.prev = $('#prev').bind('click', self.tabPrev);
      self.els.legal = $('#btn_legal');
      self.els.inputs = $(':input', self.els.el);
      self.els.inputs_text = $(':text').onlyNumber();
      self.els.text1 = $('#texttipocambio');

      /* mascara */
      self.numberMask = new Mask('#,###', 'number');
      self.els.inputs_text.each(function() {
        this.value = '0';
        self.numberMask.attach(this);
      });

      /* ahorro mensual */
      self.ahorro_mensual = 0;
      self.sueldo_bruto = 0;
      self.sueldo_libre = 0;
      self.destinado_a_credito = 0;
      self.valor_casa = 0;
      self.inicial_casa = 0;
      self.gasto_cierre = 695;
      self.monto_financiar = 0;
      self.moneda_prefix = '';

      /* converciones */
      self.els.text1.html(self.PEN_PREFIX + self.oneUSDtoPEN);

      /* gastos de cierre */
      self.tasacion = self.TASACION;
      self.notariales = self.NOTARIALES;
      self.gastos_cierre = 0;
      self.gastos_registrales = 0;
      //self.cierre = self.tasacion + self.notariales;


      /* Chart de gastos frente a sueldo bruto */
      self.els.chart1 = $('#chart1').charts({
        items: {
          'ahorro': {
            label: '<span class="text"></span> Ahorro',
            static: 1
          },
          'credito': 'Cr&eacute;dito hipotecario',
          'i71': 'Alimentaci&oacute;n',
          'i72': 'Educaci&oacute;n',
          'i73': 'Movilidad',
          'i74': 'Servicios',
          'i75': 'Otros',
          'i82': 'Gastos en cr&eacute;ditos',
          'inpuestos': 'Impuestos'
        }
      });
      self.chart1Data = self.els.chart1.data('chartData');
      self.chart1Data.inputs = $('#i71, #i72, #i73, #i74, #i75');
      self.chart1Data.inputsUpdateAll = function(extra) {
        var total = 100;
        var percent;

        self.chart1Data.inputs.each(function() {
          percent = self.val(this) * 100 / self.sueldo_bruto;
          total -= percent;
          self.chart1Data.update(
              this.id,
              percent.toFixed(1)
          );
        });

        total -= 30;
        self.chart1Data.update('inpuestos', '30.0');


        if (total < 0) {
          calculator.chart1Data.el.find('.label-item-ahorro').hide();
        }

        else {
          if (extra) {
            total -= parseFloat(extra);
          }

          calculator.chart1Data.el.find('.label-item-ahorro').show()
            .find('.text').html(
              total.toFixed(1) +
              '% '
              );
        }
      };

      self.chart1Data.inputs.bind('keyup change', function(e) {
        self.chart1Data.inputsUpdateAll();
      });





      /* Chart de credito hipotecario frente sueldo */
      self.els.chart2 = $('#chart2').charts({
        items: {
          'valor_casa': {
            label: '<span class="text"></span> Valor de la casa',
            static: 1
          },
          'cuota_inicial': {
            label: '<span id="cuota_inicial"></span> Cuota inicial ',
            row: true
          },
          'cierre': {
            label: '<span id="charttext">S/. 695</span> * Gastos de cierre ',
            row: true
          }
        }
      });
      self.chart2Data = self.els.chart2.data('chartData');






      /* chart de ahorro y credito conocidos por el usuario */
      self.els.chart3 = $('#chart3').charts({
        type: 2,
        items: {
          'sueldo': {
            label: '<span class="text"></span>  Sueldo bruto',
            static: 1
          },
          'ahorro': 'Ahorro',
          'destinado_a_credito': 'Cr&eacute;dito hipotecario'
        }
      });
      self.chart3Data = self.els.chart3.data('chartData');
      self.chart3Data.setGrossSalary = function() {
        calculator.chart3Data.el.find('.label-item-sueldo')
          .show()
          .find('.text').html(
            calculator.p5.getMoneyPrefix() +
            calculator.addCommas(calculator.p5.getGrossSalary().toFixed(1)) +
            ' '
            );
      }






      /* Al mostrar todas las calculadoras evaluo se debo mostrar
       * los charts para cada paso asignado */
      self.els.contents.bind('calculator:show', function() {
        var el = $(this);

        try {
          if (self[this.id].is_init === 0) {
            self[this.id].init();
          }
        }

        catch (e) {
          throw 'No hay metodos para el paso ' + el.id;
        }


        $('.charts').css('left', '-99999em');
        self.els.legal.hide();

        if (parseInt(el.attr('id').split('p').join(''), 10) > 1) {
          self.els.aviso.fadeIn();
        }
        else {
          self.els.aviso.fadeOut();
        }

        if (el.is('#p6')) {
          self.els.chart3.css('left', 25);
        }

        else if (el.is('#p7') || el.is('#p8')) {
          //self.els.chart1.show();
          self.els.chart1.css('left', 25);
        }

        else if (el.is('#p9')) {
          if (
              parseInt(self.p6.els.radios.filter(':checked').val(), 10) === 1
          ) {
            //self.els.chart3.show();
            self.els.chart3.css('left', 25);
          }

          else {
            //self.els.chart1.show();
            self.els.chart1.css('left', 25);
          }
        }

        else if (el.is('#p10') || el.is('#p11')) {
          //self.els.chart2.show();
          self.els.chart2.css('left', 10);
        }
      });

      //paso 6, capacidad de ahorro
      $('#p6').bind('calculator:show', function() {
        var type = self.p5.getTypeMoney();
        self.sueldo_bruto = self.p5.getGrossSalary();
      });

      //paso 7, capacidad de ahorro
      $('#p7').bind('calculator:show', function() {
        self.els.next.removeClass('inactive');
        self.p7.ahorroMensual();

        // recojo el sueldo bruto mensual
        var impuestos = self.impuestos;

        self.p7.els.text1.html(
            self.moneda_prefix + self.addCommas(self.sueldo_bruto.toFixed(1))
        );

        self.p7.els.text3.html(
            self.moneda_prefix + self.addCommas(impuestos.toFixed(1))
        );

        // impuestos, solo cuando inicia el tab
        calculator.chart1Data.update('inpuestos', '30.0');
      });

      $('#p8').bind('calculator:show', function() {
        if (calculator.p6.els.input2.is(':checked')) {
          calculator.p8.gastosCreditos();
        }
        self.els.next.removeClass('inactive');
      });

      $('#p9').bind('calculator:show', function() {
        var percent;

        if (
          calculator.p6.els.radios.filter(':checked').val() === '0' &&
          calculator.val(calculator.p9.els.input)
        ) {
            // nada en especial  
        }
        else {
          if (calculator.p6.els.radios.filter(':checked').val() === '0') {
            calculator.p8.gastosCreditos();
          }

          self.p9.els.text1.html(
              self.moneda_prefix +
              self.addCommas(self.ahorro_mensual.toString())
          );

          /* sabe su ahorro mensual */
          if (parseInt(self.p6.els.radios.filter(':checked').val(), 10) === 1) {
            percent = (self.destinado_a_credito * 100) /
                self.sueldo_bruto;

            self.p9.els.text2.html(percent.toFixed(1));
            self.p9.els.text4.html(percent.toFixed(1));
            self.p9.els.text6.html(
              self.moneda_prefix +
              self.addCommas(self.destinado_a_credito.toFixed(1))
            );

            /* en la moneda de credito */
            if (calculator.p3.convertAccordingType(1) !== 1) {
              self.p9.els.group5.show();
              self.p9.els.text5.html(
                  self.p3.getMoneyPrefix() +
                  self.addCommas(
                  self.p3.convertAccordingType(
                  self.ahorro_mensual.toFixed(1)
                  ).toString()
                  )
              );
            }
            else {
              self.p9.els.group5.hide();
            }

            self.p9.els.input.val(self.destinado_a_credito);
            self.p9.els.input.trigger('blur');

            self.p9.els.group2.show();
            self.p9.els.group3.hide();
          }

          /* no sabe su ahorro mensual */
          else {
            percent = (self.ahorro_mensual * 100) /
                self.sueldo_bruto;

            /* por defecto el ahorro mensual es destinado como lo destinado
             * al credito si este no supera el 30% del ahorro */
            self.destinado_a_credito = parseInt(self.ahorro_mensual, 10);

            /* en la moneda de credito */
            if (calculator.p3.convertAccordingType(1) !== 1) {
              self.p9.els.group5.show();
              self.p9.els.text5.html(
                  self.p3.getMoneyPrefix() +
                  self.addCommas(
                  self.p3.convertAccordingType(
                      self.destinado_a_credito.toFixed(1)
                  ).toString()
                  )
              );
            }
            else {
              self.p9.els.group5.hide();
            }

            self.p9.els.text2.html(
                percent.toFixed(1)
            );
            self.p9.els.text6.html(
                self.moneda_prefix +
                self.addCommas(self.destinado_a_credito.toFixed(1))
            );

            self.p9.els.text4.html(percent.toFixed(1));
            self.p9.els.input.val(self.destinado_a_credito);
            self.p9.els.input.trigger('blur');


            self.p9.els.group2.hide();
            self.p9.els.group3.show();
          }
        }


        /* Si el porcentaje de ahorro mensaual es mayor al 30% se muestra el
         * campo para modificarlo de lo contrario se oculta */
        
        if (percent < 0) {
          self.p9.els.group6.show();
          self.p9.els.group7.hide();
          calculator.els.next.addClass('inactive');
        }

        else {
          self.p9.els.group6.hide();
          self.p9.els.group7.show();
        }

        if (percent > 30) {
          self.p9.els.group1.show();
          self.p9.els.input.trigger('change blur');
          self.els.next.addClass('inactive');

          calculator.player.sound('9-2.mp3');
        }

        else if (percent > 0) {
          self.p9.els.group1.hide();
          self.p9.els.input.trigger('change blur');
          self.els.next.removeClass('inactive');

          calculator.player.sound('9.mp3');
        }

        self.p9.percent = percent;
      });

      $('#p10').bind('calculator:show', function() {
        self.p10.gastosRegistrales();

        if (self.p3.getTypeMoney() === 'soles') {
          self.p10.els.el.addClass('soles').removeClass('dolares');
        }

        else {
          self.p10.els.el.addClass('dolares').removeClass('soles');
        }


        calculator.player.sound('10.mp3');
      });

      $('#p11').bind('calculator:show', function() {
        if (calculator.p6.els.input2.is(':checked')) {
          calculator.p8.gastosCreditos();
        }

        calculator.p11.is_success = 0;
        calculator.p11.is_error = 0;

        self.monto_financiar = self.valor_casa - self.cuota_inicial;

        calculator.player.sound('11.mp3');

        calculator.p11.displayCuotaMensual(calculator.p11.anios_default);


        if (calculator.p3.getTypeMoney() === 'soles') {
          self.p11.els.el.addClass('soles').removeClass('dolares');
          self.p11.els.text7.html(
              //self.PEN_PREFIX +
              self.addCommas(self.monto_financiar)
          );

          self.p11.els.text2.html(
              //self.PEN_PREFIX +
              self.addCommas(self.monto_financiar)
          );

          self.p11.els.input.val(
              self.addCommas(self.monto_financiar)
          );

        }

        else {
          self.p11.els.el.addClass('dolares').removeClass('soles');
          self.p11.els.text7.html(
              //self.USD_PREFIX +
              self.addCommas(self.monto_financiar)
          );

          self.p11.els.text2.html(
              //self.USD_PREFIX +
              self.addCommas(self.monto_financiar)
          );

          self.p11.els.input.val(
              self.addCommas(self.monto_financiar)
          );
        }
      });

      self.player.init();

      return self;
    }
  }.init();
});
