$(function() {
  var form = {
    e: {},
    init: function() {
      var self = this;
      var query = location.search.split('?').join('');

      if (query) {
        query = query.split('&');
        query[0] = query[0].split('=')[1] || '';
        query[1] = query[1].split('=')[1] || '';

        self.e.el = $('#form');
        $('#link').val(query[0])
        $('#type').val(query[1])
      }

      return self;
    }
  }.init();
});
