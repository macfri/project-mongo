$(function() {
  var accordion_title = $('.accordion-title');

  accordion_title.bind('click', function(e) {
    e.preventDefault();

    var el = $(this);
    $('.accordion-title-open').removeClass('accordion-title-open').next().hide();
    el.addClass('accordion-title-open').next().show();
  });

  var autoshow = {
    init: function() {
      var self = this;
      var query = location.hash;

      if (query) {
        params = query.split('/');
        params.shift();

        $('#' + params[0]).trigger('click');
      }

      return self;
    }
  }.init();


  var enviar = {
    e: {},

    show: function(obj) {
      var self = this;
      obj = obj || {};

      self.e.email.val('');
      self.e.el.find('.error').hide();
      self.e.el.find('.text').hide();
      self.rel = obj.rel || '';

      setTimeout(function() {
        self.e.el.reveal({
          animation: 'fade'
        });
      }, 200);
    },

    validate: function() {
      var self = this;
      var status = 0;
      self.e.el.find('.error').hide();

      if (self.e.email.val() === '') {
        status = 1;
        self.e.error2.show();
      }

      else if (
          !/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
        .test(self.e.email.val())
      )
      {
        status = 1;
        self.e.error1.show();
      }


      return status;
    },

    send: function(e) {
      e.preventDefault();

      var self = enviar;
      self.e.el.find('.text').hide();

      if (self.validate() !== 0) {
        return;
      }

      var url = 'http://' + location.host + '/recommend';

      self.e.text1.show();

      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: {
          type: 'seguros',
          link: self.rel,
          email: self.e.email.val()
        },
        success: function(r) {
          if (r.status_code === 0) {
            self.e.text1.hide();
            self.e.text2.show();
            self.e.email.val('');
          }
          else {
            self.e.el.find('.text').hide();
            self.e.error3.show();
          }
        },

        error: function() {
          self.e.el.find('.text').hide();
          self.e.error3.show();
        }
      });
    },

    init: function() {
      var self = this;
      self.e.el = $('#sendmail');
      self.e.form = $('#sendmail_form').bind('submit', self.send);
      self.e.email = $('#sendmail_email');
      self.e.error1 = $('#sendmail_error1');
      self.e.error2 = $('#sendmail_error2');
      self.e.error3 = $('#sendmail_error3');
      self.e.text1 = $('#sendmail_text1');
      self.e.text2 = $('#sendmail_text2');
      return self;
    }
  }.init();

  $('.send-email-now').bind('click', function(e) {
    e.preventDefault();
    var el = $(this);
    enviar.show({
      rel: el.attr('rel')
    });
  });
});
