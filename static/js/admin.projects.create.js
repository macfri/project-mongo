$(function() {

  // formulario
  var form = {

    elements: {},

    rules: {
      title: {
        required: true
      },

      price: {
        required: true
      },

      address: {
        required: true
      },

      //description: {
        //required: true
      //},

      propietorship_for: {
        required: true
      },

      department: {
        required: true
      },

      province: {
        required: true
      },

      district: {
        required: true
      },

      contact_email: {
        email: true,
        required: true
      },

      contact_name: {
        required: true
      },

      contact_address: {
        required: true
      },

      contact_phone: {
        required: true
      },

      specs_web: {
        url: true
      },

      ubigeo: {
        required: true
      },

      province: {
        required: true
      },

      department: {
        required: true
      },

      type: {
        required: true
      }
    },

    keyOnly: function(e, type, special) {
      /*
        backspace = 8
        supr = 46
        tab = 9
        flechas direccionales = 37 al 40
      */
      var allowed = '';
      var characters = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
      var numbers = '0123456789';
      var arrows = false;
      var keyCode = e.charCode || e.keyCode;
      var caracter = String.fromCharCode(keyCode);

      switch (type) {
        case 1:
          allowed = numbers;
          break;
        case 2:
          allowed = characters;
          break;
        case 4:
          allowed = characters + numbers;
          break;
      }

      if (special) {
        allowed = allowed + special;
      }

      if ($.browser.mozilla) {
        arrows = keyCode > 36 && keyCode < 41;
      }

      return allowed.indexOf(caracter) != -1 ||
          e.keyCode != 0 &&
          (
          keyCode == 8 ||
          keyCode == 9 ||
          keyCode == 13 ||
          arrows
          );
    },

    init: function() {
      var self = this;
      self.elements.form = $('#form');

      self.elements.form.validate({
        rules: self.rules,
        errorElement: 'small'
      });

      $('.number').bind('keypress', function(event) {
        if (self.keyOnly(event, 1, '.') === false) {
          event.preventDefault();
        }
      });
    }

  };

  form.init();


  // multipel inputs file
  var file = {

    remove: function(event) {
      event.preventDefault();
      var self = this,
          el = $(this);

      el.unbind('click', self.remove);
      el.parent().remove();
    },

    add: function(event) {
      event.preventDefault();
      var self = this,
          el = $(this),
          target = $('#' + el.attr('rel')),
          clone = target.children(':first').clone(false);

      clone.find('input').val('');
      clone.find('.input-remove').show().bind('click', file.remove);

      target.append(clone);
    },

    init: function() {
      var self = this;
      var el = $('.input-add');

      el.bind('click', self.add);
    }
  };

  file.init();


  // ubigeo
  var ubigeo = {

    elements: {},

    tplOption: function(data) {
      return '<option value="' + data.value + '">' + data.text + '</option>';
    },

    change: function(opt) {
      $.getJSON(opt.url, function(r) {
        var html = ubigeo.tplOption({
          text: 'Elija una opci\xf3n',
          value: ''
        });

        $.each(r.data, function(i, o) {
          html += ubigeo.tplOption({
            value: o[opt.value_key],
            text: o['name']
          });
        });

        opt.target.html(html);
      });
    },

    depChange: function() {
      var el = $(this);
      var code = el.val();

      ubigeo.change({
        url: urls.provinces.split('%id1%').join(code),
        target: ubigeo.elements.pro,
        value_key: 'province_code'
      });

      ubigeo.elements.dis.html(ubigeo.tplOption({
        text: 'Elija una provincia'
      }));
    },

    proChange: function() {
      var el = $(this);
      var dep_code = ubigeo.elements.dep.val();
      var code = el.val();

      ubigeo.change({
        url: urls.districts.split('%id1%').join(dep_code).split('%id2%').join(code),
        target: ubigeo.elements.dis,
        value_key: 'id'
      });
    },


    init: function() {
      var self = this;

      self.elements.dep = $('#department').bind('change', self.depChange);
      self.elements.pro = $('#province').bind('change', self.proChange);
      self.elements.dis = $('#district');
    }

  };

  ubigeo.init();

  // imagenes
  var gall = {

    elements: {},

    deleteImage: function(e) {
      e.preventDefault();

      var el = $(this);
      var cont = $('#' + el.attr('data-index'));

      cont.fadeTo('fast', 0.5);
      cont.find('.delete-image').hide();

      gall.elements.modal.trigger('reveal:close');

      $.ajax({
        url: el.attr('href'),
        type: 'post',
        dataType: 'json',
        data: {
          _xsrf: readCookie('_xsrf'),
          image_id: el.attr('data-id'),
          project_id: el.attr('data-project_id')
        },
        success: function(r) {
          if (r.status_code === 0) {
            cont.hide(0, function() {
              cont.remove();
            });
          }
          else {
            cont.fadeTo('fast', 1);
            cont.find('.delete-image').show();
          }
        },
        error: function() {
          cont.fadeTo('fast', 1);
          cont.find('.delete-image').show();
        }
      });

    },

    hideButtonDelete: function() {
      var el = $(this);
      $('.delete-image').fadeIn();

      if (el.is(':checked')) {
        $('#' + el.attr('data-delete-image')).fadeOut();
      }

    },

    init: function() {
      var self = this;

      self.elements.modal = $('#imageRemoveModal');

      self.elements.modal.find('#modal_go').bind('click', self.deleteImage);
      self.elements.modal.find('.close-reveal-modal')
        .bind('click', function(e) {
            e.preventDefault();
          });

      $('.delete-image').click(function(e) {
        e.preventDefault();
        var el = $(this);
        self.elements.modal.find('#modal_go')
          .attr('data-index', el.attr('data-index'))
          .attr('data-id', el.attr('data-id'))
          .attr('data-project_id', el.attr('data-project_id'))
          .attr('href', el.attr('href'));
        self.elements.modal.reveal();
      });

      self.hideButtonDelete.call(
          $('.image-selected')
        .bind('click', self.hideButtonDelete)
        .filter(':checked')
      );
    }

  };

  gall.init();

});
