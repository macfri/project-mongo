(function($) {
  var log_count = 0;
  function log(text) {
    var l = $('#log_');

    if (l.length === 0) {
      l = $('<div/>', {
        id: 'log_xxxxx',
        css: {
          background: 'yellow',
          padding: '10px',
          position: 'absolute',
          top: 0
        }
      });
      $('body').append(l);
    }

    if (typeof text === 'object') {
    }

    l.append('<p>'+ text +'</p>');
  }

  var hideColecctor = '';

  function checkOption() {
    this.each(function() {
      var el = $(this);
      if (el.is(':checked')) {
        onClick.call(el);
      }
    });

    return this.bind('click', onClick);

    function onClick(e) {
      var el = $(this);
      var input = el.find(':input');

      if (el.is(':radio')) {
        $('[name="' + el.attr('name') + '"]').parent().removeClass('checked');
        el.parent().addClass('checked');
      }

      else if (el.is(':checkbox')) {
        if (el.is(':checked')) {
          el.parent().addClass('checked');
        }

        else {
          el.parent().removeClass('checked');
        }
      }
    }
  }

  $.fn.checkOption = checkOption;


  function dropDown() {
    this.each(prepare);

    this.find('.drop-input').bind('click', openList);

    return this;

    function openList() {
      var el = $(this).parent();
      var data = el.data('dropData');

      $('.drop-list').css('visibility','hidden');
      hideColecctor = hideColecctor.split(' .drop-list').join('');


      if (data.list.css('visibility') === 'visible') {
        data.list.css('visibility', 'hidden');
      }

      else {
        var css = {
        };

        if (el.hasClass('dropdown-bottom')) {
          css.top = -data.list.height();
        }

        if (data.list.height() > 150) {
          css.height = 150;
          css.overflow = 'auto';
        }

        data.list.css(css).css({
          visibility: 'visible'
        });

        setTimeout(function() {
          hideColecctor += ' .drop-list';
        }, 100);
      }
    }

    function setValue() {
      var el = $(this);
      if (el.is(':input') || el.length === 0) {
        return;
      }

      var list = el.parents('.drop-list');
      var input = el.find(':input');

      list.prev().find('.input').html(
        input.attr('data-text')
      );

      list.find(':checked').each(function() {
        $(this).removeAttr('checked').parent().removeClass('checked');
      });

      $('[name="' + input.attr('name') + '"]').parent().removeClass('checked');
      el.addClass('checked');
      input.attr('checked','checked');

      list.parent().trigger('dropDown:setValue');
    }

    function prepare() {
      var data = {};
      var el = $(this).data('dropData', data);

      el.find('.drop-option').live('click', setValue);
      data.list = el.find('> .drop-list');
      data.list.data('dropParent', el);
      data.setValue = function(value) {
        setValue.call(this.list.find('[value="' + value + '"]').attr('checked','checked').parent());
      };
      data.getValue = function() {
        return el.find(':checked').attr('value');
      };

      setValue.call(data.list.find(':checked'));
    }
  }

  $.fn.dropDown = dropDown;

  function keyonly(e, type, special) {
    /*
      backspace = 8
      supr = 46
      tab = 9
      flechas direccionales = 37 al 40
    */
    var allowed = '';
    var characters = 'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    var numbers = '0123456789';
    var arrows = false;
    var keyCode = e.charCode || e.keyCode;
    var caracter = String.fromCharCode(keyCode);

    switch (type) {
      case 1:
        allowed = numbers;
        break;
      case 2:
        allowed = characters;
        break;
      case 4:
        allowed = characters + numbers;
        break;
    }

    if (special) {
      allowed = allowed + special;
    }

    if ($.browser.mozilla) {
      arrows = keyCode > 36 && keyCode < 41;
    }

    return allowed.indexOf(caracter) != -1 ||
        e.keyCode != 0 &&
        (
        keyCode == 8 ||
        keyCode == 9 ||
        keyCode == 13 ||
        arrows
        );
  }


  function keyOnlyNumber() {
    return this.bind('keypress', function(event) {
      if (keyonly(event, 1) === false) {
        event.preventDefault();
      }
    });
  }

  $.fn.onlyNumber = keyOnlyNumber;

  function keyOnlyCharacters(special) {
    return this.bind('keypress', function(event) {
      if (keyonly(event, 2, 'áéíóúñ') === false) {
        event.preventDefault();
      }
    });
  }

  $.fn.onlyText = keyOnlyCharacters;

  

  $(function() {
    // ocultar al hacer click sobre el body
    $('body').click(function() {
      $(hideColecctor).css('visibility','hidden');
      hideColecctor = '';
    });
  });
} (jQuery));
