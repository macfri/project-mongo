/*
 * jQuery Reveal Plugin 1.0
 * www.ZURB.com
 * Copyright 2010, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
*/


(function($) {

/*---------------------------
  Defaults for Reveal
----------------------------*/

/*---------------------------
 Listener for data-reveal-id attributes
----------------------------*/

  $('[data-reveal-id]').live('click', function(e) {
    e.preventDefault();
    var modalLocation = $(this).attr('data-reveal-id');
    $('#' + modalLocation).reveal($(this).data());
  });

  var win = $(window);
  var doc = $(document);
  var bg = $('.reveal-modal-bg');
  var timer;

  function resizeModalBG() {
    var h = win.height();
    var w = win.width();
    var doc_h = doc.height();
    var doc_w = doc.width();

    if (bg.length === 0) {
      bg = $('.reveal-modal-bg');
    }


    if (doc_h > h) {
      bg.height(doc_h);
    }
    else {
      bg.height(h);
    }

    if (doc_w > w) {
      h = doc_w;
      bg.width(doc_w);
    }

    else {
      bg.width(w);
    }
  }

  win.resize(function() {
    clearTimeout(timer);
    timer = setTimeout(function() {
      resizeModalBG();
    }, 100);
  });

/*---------------------------
 Extend and Execute
----------------------------*/

  $.fn.reveal = function(options) {


    var defaults = {
      animation: 'fadeAndPop', //fade, fadeAndPop, none
      animationspeed: 300, //how fast animtions are
      closeonbackgroundclick: true, //if you click background will modal close?
      dismissmodalclass: 'close-reveal-modal' //the class of a button or element that will close an open modal
    };

    //Extend dem' options
    var options = $.extend({}, defaults, options);

    var body = $('#body');

    return this.each(function() {

/*---------------------------
 Global Variables
----------------------------*/
      var modal = $(this),
          topMeasure = parseInt(modal.css('top')),
          topOffset = modal.height() + topMeasure,
          locked = false,
          modalBG = $('.reveal-modal-bg');

/*---------------------------
Create Modal BG
----------------------------*/
      if (modalBG.length == 0) {
        modalBG = $('<div class="reveal-modal-bg" />').css({
            position: 'absolute',
            top: 0,
            left: 0,
            display: 'none'
        }).insertAfter(modal);

        modalBG.bind('reveal:open', function() {
          resizeModalBG();
        });
      }

/*---------------------------
 Open & Close Animations
----------------------------*/
      //Entrance Animations
      modal.bind('reveal:open', function() {
        body.addClass('modal-open');
        modal.addClass('reveal-open');
        modalBG.unbind('click.modalEvent');
        $('.' + options.dismissmodalclass).unbind('click.modalEvent');
        if (!locked) {
          lockModal();
          if (options.animation == 'fadeAndPop') {
            modal.css({'top': $(document).scrollTop() - topOffset, 'opacity' : 0, 'visibility' : 'visible'}).show();
            modalBG.fadeTo(options.animationspeed / 2, 0.6);
            modal.delay(options.animationspeed / 2).animate({
              'top': $(document).scrollTop() + topMeasure + 'px',
              'opacity' : 1
            }, options.animationspeed, unlockModal());
          }
          if (options.animation == 'fade') {
            //modal.css({'opacity' : 0, 'visibility' : 'visible', 'top': $(document).scrollTop() + topMeasure}).show();
            modal.css({'opacity' : 0, 'display' : 'block', 'top': $(document).scrollTop() + topMeasure}).show();
            modalBG.fadeTo(options.animationspeed / 2, 0.6);
            modal.delay(options.animationspeed / 2).animate({
              'opacity' : 1
            }, options.animationspeed, unlockModal());
          }
          if (options.animation == 'none') {
            modal.css({'visibility' : 'visible', 'top': $(document).scrollTop() + topMeasure});
            modalBG.css({'display': 'block'});
            unlockModal();
          }
        }
        modal.unbind('reveal:open');
        modalBG.trigger('reveal:open');
        //resizeModalBG();
      });

      //Closing Animation
      modal.bind('reveal:close', function() {
        body.removeClass('modal-open');
        modal.removeClass('reveal-open');
        if (!locked) {
          lockModal();
          if (options.animation == 'fadeAndPop') {
            modalBG.delay(options.animationspeed).fadeOut(options.animationspeed);
            modal.animate({
              'top': $(document).scrollTop() - topOffset + 'px',
              'opacity' : 0
            }, options.animationspeed / 2, function() {
              modal.css({'top': topMeasure, 'opacity' : 1, 'visibility' : 'hidden'}).hide();
              unlockModal();
            });
          }
          if (options.animation == 'fade') {
            modalBG.delay(options.animationspeed).fadeOut(options.animationspeed);
            modal.animate({
              'opacity' : 0
            }, options.animationspeed, function() {
              //modal.css({'opacity' : 1, 'visibility' : 'hidden', 'top' : topMeasure}).hide();
              modal.css({'opacity' : 1, 'display' : 'none', 'top' : topMeasure}).hide();
              unlockModal();
            });
          }
          if (options.animation == 'none') {
            modal.css({'visibility' : 'hidden', 'top' : topMeasure}).hide();
            modalBG.css({'display' : 'none'});
          }
        }
        modalBG.trigger('reveal:close');
        modal.unbind('reveal:close');
      });

/*---------------------------
 Open and add Closing Listeners
----------------------------*/
      //Open Modal Immediately
      modal.trigger('reveal:open');

      //Close Modal Listeners
      var closeButton = $('.' + options.dismissmodalclass).bind('click.modalEvent', function() {
        modal.trigger('reveal:close');
      });

      if (options.closeonbackgroundclick) {
        modalBG.css({'cursor': 'pointer'});
        modalBG.bind('click.modalEvent', function() {
          modal.trigger('reveal:close');
        });
      }
      $('body').keyup(function(e) {
        if (e.which === 27) { modal.trigger('reveal:close'); } // 27 is the keycode for the Escape key
      });


/*---------------------------
 Animations Locks
----------------------------*/
      function unlockModal() {
        locked = false;
      }
      function lockModal() {
        locked = true;
      }

    });//each call
  }//orbit plugin call
})(jQuery);


