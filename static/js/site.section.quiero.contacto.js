$(function() {
  var form = {
  
    elements: {},

    rules: {
      name: {
        required: true
      }
      , lastname_father: {
        required: true
      }
      , lastname_mother: {
        required: true
      }
      , document_number: {
        required: true
      }
      , document_type: {
        required: true
      }
      , cuenta_sueldo: {
        required: true
      }
      , email: {
        required: true,
        email: true
      }
      , phone: {
        required: true
      }
      , buying_home: {
        required: true
      }
      , buying_home_first: {
        required: true
      }
    },

    getElements: function() {
      var self = this;
      var context = self.elements.el;

      return {
        name: $('#name', context),
        lastname_father: $('#lastname_father', context),
        lastname_mother: $('#lastname_mother', context),
        document_number: $('#document_number', context),
        document_type: $('[name="document_type"]:checked', context),
        cuenta_sueldo: $('[name="cuenta_sueldo"]:checked', context),
        contact: $('[name="contact"]:checked', context),
        email: $('#email', context),
        phone: $('#phone', context),
        address: $('#address', context),
        buying_home: $('[name="buying_home"]:checked', context),
        buying_home_first: $('[name="buying_home_first"]:checked', context),
        comment: $('#comment', context)
      };
    },

    reset: function() {
      var self = this;
      $.each(self.getElements(), function(i, o) {
        if (o.is(':checkbox') || o.is(':radio')) {

          if (!o.parent().parent().hasClass('drop-list')){
            o.parent().removeClass('checked');
          }

          o.removeAttr('checked');
        }

        else {
          o.val('');
        }
      });
    },

    getValues: function() {
      var self = this;
      var context = self.elements.el;
      var values = {};

      $.each(self.getElements(), function(i, o) {
        values[i] = o.val();
      });

      return values;
    },

    submit: function(f) {
      var err = $('#error').hide();
      var suc = $('#success').hide();
      var load = $('#loading').show();

      $.ajax({
        type: 'post',
        dataType: 'json',
        url: form.elements.el.attr('action'),
        data: form.getValues(),
        success: function(r) {
          load.hide();
          if (r.status_code === 0) {
            suc.show(); 
            form.reset();
          }
          else {
            err.show();
          }
        },
        error: function() {
          load.hide();
          err.show();
        }
      });
    },

    init: function() {
      var self = this;
      self.elements.el = $('#form');
      
      self.elements.el.validate({
        debug: true,
        rules: self.rules,
        errorPlacement: function(error, element) {
          error.insertAfter(element.parents('.input-frame'));
        },
        submitHandler: self.submit
      });
      return self;
    }
  }.init();
});
