$(function() {
  var listado = {

    elements: {},
    
    remove: function(e) {
      e.preventDefault();
      var el = $(this);

      $.ajax({
        url: el.attr('href'),
        type: 'post',
        dataType: 'json',
        data: {
          _xsrf: readCookie('_xsrf'),
          project_id: el.attr('data-id')
        },
        success: function(r) {
          if (r.status_code === 0) {
            listado.elements.modal.trigger('reveal:close');
            location.reload();
          }
          else {
            listado.elements.modal.trigger('reveal:close');
            listado.elements.fail.reveal();
          }
        },

        error: function() {
          listado.elements.modal.trigger('reveal:close');
          listado.elements.fail.reveal();
        }
      });
    },

    init: function() {
      var self = this;
      self.elements.modal = $('#projectRemoveModal');

      self.elements.fail = $('#projectRemoveModalError');

      self.elements.modal.find('#modal_go').bind('click', self.remove);

      $('.close-reveal-modal').bind('click', function(e) {
        e.preventDefault();
      });


      $('.delete').click(function(e) {
        e.preventDefault();
        var el = $(this);

        self.elements.modal.find('#modal_go')
          .attr('data-index', el.attr('data-index'))
          .attr('data-id', el.attr('data-id'))
          .attr('href', el.attr('href'));

        self.elements.modal.find('#modal_name').html(el.attr('data-name'));

        self.elements.modal.reveal();
      });
    }
  };

  listado.init();
	
});
