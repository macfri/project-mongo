# -*- coding: utf-8 -*-
import os
import subprocess
import settings
from settings import *
from settings import _local_path


def render_html(template, **kwargs):
    return TEMPLATE_ENV.get_template(template).render(**kwargs)


def generate_pdf(fname, size='A5', root=None):
    if not root:
        root = PDF_ROOT

    subprocess.call(['wkhtmltopdf-amd64',
                     '-L', '0', '-T', '0',
                     '-R', '0', '-B', '0',
                     '-s', size,
                     '/tmp/%s.html' % fname,
                     os.path.join(root, '%s.pdf' % fname)])


estados_civiles = (
    (
        'Soltero(a)/Viudo(a)/Divorciado(a)',
        'soltero_viudo_divorciado',
    ),
    (
        'Casado(a)',
        'casado',
    ),
    (
        'Conviviente',
        'conviviente',
    ),
)

ingresos = (
    (
        u'Dependiente',
        ('sustento_5', 'cta_bcp'),
        'dependiente',
    ),
    (
        u'Profesional o Técnico',
        ('sustento_4', 'cta_bcp'),
        'profesional',
    ),
    (
        u'Pyme',
        ('sustento_pyme',),
        'pyme',
    ),
    (
        u'Empresario Consolidado',
        ('sustento_consolidado',),
        'empresario',
    ),
    (
        u'Accionista de Empresas',
        ('sustento_2da',),
        'accionista',
    ),
    (
        u'Arrendador de propiedades',
        ('sustento_1ra', 'cta_bcp'),
        'arrendador',
    ),
    (
        u'Genero ingresos y no los puedes sustentar',
        ('genero_ingresos', 'cta_bcp'),
        'generador',
    ),
    (
        u'Recibo remesas del exterior',
        ('remesas', 'cta_bcp'),
        'remesas',
    ),
)

tipos_vivienda = (
    ('Inmueble Terminado o Terreno', 'todos'),
    ('Proyecto Financiado por el BCP', 'bcp'),
    ('Proyecto Financiado por otros bancos o autofinanciado', 'todos'),
)


for ingreso in ingresos:
    for estado in estados_civiles:
        for vivienda in tipos_vivienda:
            for mixto in (True, False):
                for tiene_casa in (True, False):
                    html = render_html(
                        'pdf/requisitos.html',
                        media=STATIC_PATH,
                        ingreso=ingreso[2],
                        estado=estado[1],
                        vivienda=vivienda[1],
                        tiene_casa=tiene_casa,
                        DOMAIN=DOMAIN,
                    )

                    fname = (ingreso[2] + '-' +
                             estado[1] + '-' +
                             vivienda[1] + '-' +
                             'tiene_casa_' + str(tiene_casa).lower())

                    if mixto:
                        fname = 'mixto-' + fname

                    with open('/tmp/%s.html' % fname, 'wb') as f:
                        f.write(html.encode('utf-8'))

                    subprocess.call(['wkhtmltopdf-amd64',
                                     '-L', '0', '-T', '0',
                                     '-R', '0', '-B', '0',
                                     '-s', 'B4',
                                     '/tmp/%s.html' % fname,
                                     #'/tmp/pdf/%s.pdf' % fname,])
                                     os.path.join(_local_path, 'pdf', 'requisitos', '%s.pdf' % fname)])

                    if mixto:
                        subprocess.call(['rm', '/tmp/%s.html' % fname])

# seguros
for fname in ('seguro_de_desgravamen', 'seguro_de_inmueble'):
    html = render_html('pdf/seguro.html', seguro=fname, media=STATIC_PATH)
    with open('/tmp/%s.html' % fname, 'wb') as f:
        f.write(html.encode('utf-8'))
    generate_pdf(fname, root=PDF_ROOT + '/seguros')

# creditos
# productos
for fname in (
        'modal_ch_tradicional',
        'modal_ch_tradicional_1',
        'modal_ch_tradicional_2',
        'modal_ch_pyme',
        'modal_ch_ahorro_local',
        'modal_ch_ahorro_local_1',
        'modal_ch_remesas',
        'modal_ch_mivivienda',
        'modal_ch_mivivienda_1',
        'modal_ch_techo_propio',
        'modal_ahorrar_local',
        'modal_ahorrar_local_2',
        'modal_ahorrar_local_conoce',
        'modal_ahorrar_remesas',
        'modal_ahorrar_remesas_2',
        'modal_ahorrar_remesas_conoce',
        'modal_ahorrar_mivivienda',
        'modal_ahorrar_mivivienda_2',
        'modal_ahorrar_mivivienda_conoce',
        'modal_ahorrar_techopropio',
        'modal_ahorrar_techopropio_conoce'):
    html = render_html('pdf/credito.html', credito=fname, media=STATIC_PATH)
    with open('/tmp/%s.html' % fname, 'wb') as f:
        f.write(html.encode('utf-8'))
    generate_pdf(fname, root=PDF_ROOT + '/creditos', size='A4')
