from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, PickleType, Boolean, DateTime


from datetime import datetime

Base = declarative_base()

class Project(Base):
    __tablename__ = 'projects'

    id = Column(Integer, primary_key=True)
    title = Column(String(100))
    areas = Column(String(150))
    characteristics = Column(PickleType)
    information = Column(PickleType)
    status = Column(Boolean(), default=False)
    created_at = Column(DateTime, default=datetime)
    modified_at = Column(DateTime, default=datetime)



class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(100))
    _password = Column(String(100))
    status = Column(Boolean(), default=False)


if __name__ == '__main__':

    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker


    engine = create_engine(
        'mysql://root:root@localhost/lider',
        echo=True
    )

    session = sessionmaker(bind=engine)
   
    session = session()
    session.execute("""drop table projects""") 

    Base.metadata.create_all(engine)
