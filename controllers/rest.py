from controllers import RequestHandler
from models import Ubigeo


class Departments(RequestHandler):

    def get(self):
        self.finish({
            'data': [dict(
                department_code=ubigeo.department_code,
                name=ubigeo.name
            ) for ubigeo in Ubigeo.objects(
                    district_code='00',
                    province_code='00',
                    department_code__ne='00'
            ).only('name', 'department_code').order_by('name')
        ]})


class Provinces(RequestHandler):

    def get(self, department_code):
        self.finish({
            'data': [dict(
                department_code=ubigeo.department_code,
                province_code=ubigeo.province_code,
                name=ubigeo.name
            ) for ubigeo in Ubigeo.objects(
                    province_code__ne='00',
                    district_code='00',
                    department_code=department_code
            ).only('name', 'department_code', 'province_code').order_by('name')
        ]})


class Districts(RequestHandler):

    def get(self, department_code, province_code):
        self.finish({
            'data': [dict(
                id=str(ubigeo.id),
                name=ubigeo.name
            ) for ubigeo in Ubigeo.objects(
                    district_code__ne='00',
                    province_code__ne='00',
                    province_code=province_code,
                    department_code=department_code
            ).only('name').order_by('name')
        ]})
