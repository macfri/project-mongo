import tornado.web
import logging

from models import User
from libs.pagination import Paginator

__all__ = ['RequestHandler', 'ListMixin']


class QueryDict(dict):

    def getlist(self, key, default=None):
        return self.get(key, default)


class RequestHandler(tornado.web.RequestHandler):

    @property
    def db(self):
        return self.application.db

    def get_current_user(self):
        _user = self.get_secure_cookie("user")
        if _user:
            return User.objects.with_id(_user)
        return None

    def render_string(self, template, **kwargs):
        kwargs.update({'handler': self})
        return self.settings.get('template_env')\
            .get_template(template).render(**kwargs)

    def render(self, template, **kwargs):
        self.finish(self.render_string(template, **kwargs))

    @property
    def request_data(self):
        return QueryDict(dict(
            (key, self.get_arguments(key)) for key in self.request.arguments))


class ListMixin(object):

    @property
    def current_page(self):
        current_page = self.get_argument('page', '1')
        return int(current_page) if current_page.isdigit() else 1

    def get_pagination(self, query, count, per_page=10):
        try:
            per_page = int(per_page)
        except ValueError:
            per_page = 10

        page = self.current_page
        paginator = Paginator(page=self.current_page, total_items=count,
                              per_page=per_page)
        per_page = paginator.per_page

        return {
            'items': query[(page - 1) * per_page: page * per_page],
            'pages': paginator.pages,
            'total_items': count,
            'current_page': page,
            'total_pages': paginator.total_pages,
        }
