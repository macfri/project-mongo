# -*- coding: utf-8 -*-
import json
import os
import logging
import html2text
from tornado.web import HTTPError
from textwrap import dedent

from controllers import *
from models import Project, Ubigeo
from util import Mail
from tasks import send_email, send_mail
from forms import SearchForm, ContactForm, RecommendForm


class List(RequestHandler, ListMixin):

    def initialize(self, externo=False):
        self.externo = externo

    def get(self):
        filters = {
            'externo' + ('' if self.externo else '__ne'): True,
            'status': True,
        }

        queryset = Project.objects.only(
                'title', 'bedrooms', 'bathrooms', 'garages',
                'description', 'ubigeo', 'slug', 'selected_image',
                'type', 'published_at', 'address', 'price', 'price_type'
        ).filter(**filters)

        form = SearchForm(self.request_data)

        if form.validate():
            logging.debug('%s' % form.data)
            if form.specs.data:
                if form.specs.data.isdigit():
                    queryset = queryset.filter(bedrooms=form.specs.data)
                else:
                    queryset = queryset.filter(bedrooms__gt=3)

            if form.pmin.data:
                queryset = queryset.filter(price__gte=form.pmin.data)

            if form.pmax.data:
                queryset = queryset.filter(price__lte=form.pmax.data)

            if form.dis.data:
                try:
                    ubigeo = [u for u in Ubigeo.objects(id=form.dis.data)]
                    queryset = queryset.filter(ubigeo__in=ubigeo)
                except Exception as exc:
                    logging.info(exc)

            if form.type.data:
                queryset = queryset.filter(type=form.type.data)

            count = queryset.count()
        else:
            logging.debug('%s' % form.errors)
            queryset = []
            count = 0

        ids_ubigeos = [x.id for x in Project.\
            objects.only('ubigeo').distinct('ubigeo')]

        _ubigeos = Ubigeo.objects(
            id__in=ids_ubigeos).order_by('province_code', '+name')

        data = []

        for x in _ubigeos:
            print x.id
            ubigeo = Ubigeo.objects(id=x.id).first()
            logging.info(ubigeo.name)
            dep = ubigeo.department_code
            pro = ubigeo.province_code
            dis = ubigeo.district_code

            data_province = Ubigeo.objects(department_code=dep,
                    province_code=pro,
                    district_code='00'
                ).first()

            data_district = Ubigeo.objects(department_code=dep,
                    province_code=pro,
                    district_code=dis
                ).first()

            data.append(
                dict(province=data_province.name,
                    district=data_district.name,
                    code=str(ubigeo.id)
                )
            )

        data = sorted(data, key=lambda y: (y['province'], y['district']))

        districts = json.dumps([dict(
            name='%s - %s' % (i['province'], i['district']),
            department_code=i['code']) for i in data])

        self.render(
            'site/quiero-comprar-una-casa/inmuebles.jinja',
            externo=self.externo,
            departments=districts,
            types=json.dumps(dict(Project.TYPES)),
            **self.get_pagination(count=count, query=queryset,
                per_page=self.get_argument('per_page', 10))
        )


class Detail(RequestHandler):

    def get(self, slug):
        project = Project.objects(slug=slug, status=True).only(
                'title', 'description', 'images', 'ubigeo', 'price',
                'selected_image', 'type', 'published_at', 'address',
                'contact', 'specs', 'characteristics', 'externo',
                'bedrooms', 'bathrooms', 'garages', 'price_type').first()

        if not project:
            raise HTTPError(404)

        self.render(
            'site/quiero-comprar-una-casa/inmueble-detalle.jinja',
            project=project)


class Contact(RequestHandler):

    def post(self):
        form = ContactForm(self.request_data)
        data = {'status_code': 0}

        if form.validate():
            mail = Mail(
                subject=u'Foo',
                sender=form.email.data,
                to=[self.settings.get('contact_email')])

            content = self.render_string('mail.html', form=form)

            content_plain = html2text.html2text(content)
            mail.add_body(content_plain, 'plain')
            mail.add_body(content, 'html')
            send_email.delay(mail)
        else:
            data['status_code'] = 1
            data['errors'] = form.errors

        self.finish(data)


class Recommend(RequestHandler):

    def post(self):

        form = RecommendForm(self.request_data)
        data = {'status_code': 0}

        if form.validate():
            type = form.type.data

            if type == 'credito':
                self.creditos(form)
            elif type == 'necesitas':
                self.requisitos(form)
            elif type == 'seguros':
                self.seguros(form)
            else:
                raise HTTPError(400)

        else:
            data['status_code'] = 1
            data['errors'] = form.errors

        self.finish(data)

    def creditos(self, form):
        # TODO restricciones
        fname = self.get_argument('id')
        subject = u'El crédito hipotecario a tu medida'
        send_mail.delay(
            subject=subject,
            from_email='info@info.info',
            to=form.email.data,
            body_message=dedent(u"""
            Descarga aquí las características del Crédito Hipotecario hecho a tu medida.
            Atentamente
            Banco de Crédito BCP
            """),
            pdf_name='el_credito_hipotecario_a_tu_medida',
            fname=os.path.join(self.settings['pdf_root'],
                               'creditos', fname + '.pdf'),
        )

    def seguros(self, form):
        if form.link.data == 'a1':
            subject = 'Seguro de desgravamen'
            fname = 'seguro_de_desgravamen'

        elif form.link.data == 'a2':
            subject = 'Seguro de inmueble'
            fname = 'seguro_de_inmueble'

        send_mail.delay(
            subject=subject,
            from_email='info@info.info',
            to=form.email.data,
            pdf_name=fname,
            body_message=dedent(u"""
            Descarga la descripción del %s aquí
            Atentamente,
            Banco de Crédito BCP
            """ % subject),
            fname=os.path.join(self.settings['pdf_root'],
                               'seguros', fname + '.pdf'),
        )

    def requisitos(self, form):
        #params = form.link.data.split('#!/')[1].split('/')
        params = form.link.data.split('/')
        estados_civiles = (
            (
                'Soltero(a)/Viudo(a)/Divorciado(a)',
                'soltero_viudo_divorciado',
            ), (
                'Casado(a)',
                'casado',
            ), (
                'Conviviente',
                'conviviente',
            ),
        )

        ingresos = (
            (
                u'Dependiente',
                ('sustento_5', 'cta_bcp'),
                'dependiente',
            ), (
                u'Profesional o Técnico',
                ('sustento_4', 'cta_bcp'),
                'profesional',
            ), (
                u'Pyme',
                ('sustento_pyme',),
                'pyme',
            ), (
                u'Empresario Consolidado',
                ('sustento_consolidado',),
                'empresario',
            ), (
                u'Accionista de Empresas',
                ('sustento_2da',),
                'accionista',
            ), (
                u'Arrendador de propiedades',
                ('sustento_1ra', 'cta_bcp'),
                'arrendador',
            ), (
                u'Genero ingresos y no los puedes sustentar',
                ('genero_ingresos', 'cta_bcp'),
                'generador',
            ), (
                u'Recibo remesas del exterior',
                ('remesas', 'cta_bcp'),
                'remesas',
            ),
        )

        tipos_vivienda = (
            ('Inmueble Terminado o Terreno', 'todos'),
            ('Proyecto Financiado por el BCP', 'bcp'),
            ('Proyecto Financiado por otros bancos o autofinanciado',
             'todos'),
        )

        table = {
            # ingresos
            'q1_1': {'ingreso': ingresos[0]},     # dependiente
            'q2-1_1': {'ingreso': ingresos[1]},   # profesional
            'q2-1_2': {'ingreso': ingresos[2]},   # pyme
            'q2-1_3': {'ingreso': ingresos[3]},   # empresario
            'q2-2_1': {'ingreso': ingresos[6]},   # generador
            'q2-2_2': {'ingreso': ingresos[7]},   # remesas
            'q2-3_1': {'ingreso': ingresos[0]},   # dependiente
            'q2-3_2': {'ingreso': ingresos[1]},   # profesional
            'q2-3_3': {'ingreso': ingresos[2]},   # pyme
            'q2-3_4': {'ingreso': ingresos[3]},   # empresario
            'q2-3_5': {'ingreso': ingresos[4]},   # accionista
            'q2-3_6': {'ingreso': ingresos[5]},   # arrendador
            # estados
            'q3_1': {'estado': estados_civiles[0]},
            'q3_2': {'estado': estados_civiles[1]},
            'q3_3': {'estado': estados_civiles[2]},
            # vivienda
            'q5_1': {'vivienda': tipos_vivienda[0]},
            'q5_2': {'vivienda': tipos_vivienda[1]},
            'q5_3': {'vivienda': tipos_vivienda[0]},
        }

        result = {
            'mixto': 'q1_4' in params,
            'tiene_casa': 'tiene_casa_' + str('q4_1' in params).lower()
        }

        [result.update(table.get(i)) for i in params if i in table]

        fname = (result['ingreso'][2] + '-' +
                 result['estado'][1] + '-' +
                 result['vivienda'][1] + '-' +
                 result['tiene_casa'] + '.pdf')

        if result['mixto']:
            fname = 'mixto-' + fname

        logging.info('pdf: %s' % fname)

        send_mail.delay(
            subject=u'Los requisitos para acceder a tu Crédito ' \
                     'Hipotecario',
            from_email='info@info.info',
            to=form.email.data,
            body_message=dedent(u"""
            Descarga Los requisitos para acceder a tu Crédito Hipotecario aquí
            Atentamente,
            Banco de Crédito BCP
            """),
            fname=os.path.join(self.settings['pdf_root'],
                               'requisitos', fname),
        )
