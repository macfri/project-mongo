from tornado import ioloop, web
from tornado.options import define, options, parse_command_line
from mongoengine import connect

import settings
from urls import urls


class _Application(web.Application):

    def __init__(self, *args, **kwargs):
        super(_Application, self).__init__(*args, **kwargs)
        self.db = connect(**kwargs['mongo'])


global_settings = dict((setting.lower(), getattr(settings, setting))
    for setting in dir(settings) if setting.isupper())

if __name__ == '__main__':
    define('host', default='127.0.0.1', help='host address to listen on')
    define('port', default=8888, type=int, help='port to listen on')

    parse_command_line()
    application = _Application(urls, **global_settings)
    application.listen(options.port, options.host)

    ioloop.IOLoop.instance().start()
