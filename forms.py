# -*- coding: utf-8 -*-
import bson
from wtforms import (Form, validators, BooleanField, TextField, IntegerField,
                     FieldList, FileField, FloatField, SelectField)
from models import Project


MSG_REQUIRED = 'Este campo es obligatorio'
MSG_LENGTH = 'El campo debe tener entre %s y %s caracteres'


class ObjectIdValidator(object):

    def __init__(self, message=None):
        self.message = message

    def __call__(self, form, field):
        try:
            bson.ObjectId(field.data)
        except bson.errors.InvalidId as exc:
            message = self.message or str(exc)
            raise validators.ValidationError(message)


class LesserOrEqual(object):

    def __init__(self, fieldname, coerce=int):
        self.fieldname = fieldname
        self.coerce = coerce

    def __call__(self, form, field):
        other = form[self.fieldname]
        if field.data not in ('', None) and other.data not in ('', None):
            other_val = self.coerce(other.data)
            this_val = self.coerce(field.data)

            if this_val > other_val:
                raise validators.ValidationError(
                        'Not lesser or equal than %s' % self.fieldname)


class RequiredIfFieldEqualTo(object):
    def __init__(self, fieldname, value, message=None):
        self.fieldname = fieldname
        self.message = message
        self.value = value

    def __call__(self, form, field):
        try:
            other = form[self.fieldname]
        except KeyError:
            raise validators.ValidationError(
                field.gettext(u"Invalid field name '%s'.") % self.fieldname)

        if field.data == self.value and not field.data:
            if self.message is None:
                self.message = field.gettext(u'Field is required')
            raise validators.ValidationError(self.message)
        else:
            raise validators.StopValidation()


class CustomBooleanField(BooleanField):

    def process_formdata(self, valuelist):
        self.data = bool(valuelist[0]) if valuelist else False


def atleast_one_value_from_list():
    message = 'at least one value from a list'

    def _atleast_one_value_from_list(form, field):
        if len(f.data) < 1:
            raise ValidationError(message)

    return _atleast_one_value_from_list

class ProjectForm(Form):
    title = TextField(
            validators=[validators.Required(message=MSG_REQUIRED)], default='')
    #price = FloatField(
            #validators=[validators.Required(message=MSG_REQUIRED)], default=0)
    #address = TextField(
            #validators=[validators.Required(message=MSG_REQUIRED)], default='')
    description = TextField(default='')
    areas = TextField(default='')
    characteristics = TextField(default='')
    information = TextField(default='')
    status = CustomBooleanField(default=False)
    type = SelectField(
            choices=Project.TYPES,
            validators=[validators.Required(message=MSG_REQUIRED)],
            coerce=int)

    #price_type = SelectField(
            #choices=Project.PRICE_TYPES,
            #validators=[validators.Required(message=MSG_REQUIRED)],
            #coerce=int)

    proccess = IntegerField(default=0)
    #bathrooms = FloatField(default=0)
    #garages = IntegerField(default=0)
    #externo = BooleanField(default=False)
    ubigeo = TextField(
            validators=[ObjectIdValidator(message=u'Opción inválida')])


"""
class SpecsForm(Form):
    builder = TextField(default='')
    num_available = IntegerField(default=0)
    floor = TextField(default='')
    area_min = FloatField(default=0)
    area_max = FloatField(default=0)
    price_min = FloatField(default=0)
    price_max = FloatField(default=0)
    web = TextField(default='')


class CharacteristicsForm(Form):
    serviceroom = BooleanField(default=False)
    gas = BooleanField(default=False)
    swimming_pool = BooleanField(default=False)
    closet = BooleanField(default=False)
    laundry = BooleanField(default=False)
    park_view = BooleanField(default=False)
    terrace = BooleanField(default=False)
    garden = BooleanField(default=False)
    furnished = BooleanField(default=False)
    kitchen_cabinet = BooleanField(default=False)
"""


class ContactAdminForm(Form):
    name = TextField(
            validators=[validators.Required(message=MSG_REQUIRED)], default='')
    phone = TextField(
            validators=[validators.Required(message=MSG_REQUIRED),
                validators.Regexp(r'(\d|-|\*)')], default='')
    email = TextField(
            validators=[validators.Required(message=MSG_REQUIRED),
                validators.Email()], default='')
    address = TextField(
            validators=[validators.Required(message=MSG_REQUIRED)], default='')


class ContactForm(Form):
    name = TextField(
            validators=[validators.Required(message=MSG_REQUIRED)],
            default='')
    lastname_mother = TextField(
            validators=[validators.Required(message=MSG_REQUIRED)],
            default='')
    lastname_father = TextField(
            validators=[validators.Required(message=MSG_REQUIRED)],
            default='')
    document_type = TextField(
            validators=[validators.Required(message=MSG_REQUIRED),
                        validators.Regexp(r'^(?i)(dni|ce)$')])
    document_number = TextField(
            validators=[validators.Required(message=MSG_REQUIRED),
                        validators.Length(min=8)])
    cuenta_sueldo = IntegerField(
            validators=[validators.NumberRange(min=0, max=1)])
    contact = TextField(
            validators=[validators.Optional()],
            default='')
    phone = TextField(
            validators=[RequiredIfFieldEqualTo('contact', 'phone'),
                        validators.Regexp(r'(\d|-|\*)')],
            default='')
    address = TextField(
            validators=[RequiredIfFieldEqualTo('contact', 'address')],
            default='')
    email = TextField(
            validators=[RequiredIfFieldEqualTo('contact', 'email'),
                        validators.Email()],
            default='')
    buying_home = IntegerField(
            validators=[validators.NumberRange(min=0, max=1)])
    buying_home_first = IntegerField(
            validators=[validators.NumberRange(min=0, max=1)])
    comment = TextField(default='')


class SearchForm(Form):
    pmin = IntegerField(validators=[LesserOrEqual('pmax')])
    pmax = IntegerField()
    dis = TextField()
    specs = TextField(
            validators=[validators.Optional(),
                        validators.Regexp(r'(\d|mastres)')])
    type = SelectField(
            choices=((None, ''),) + Project.TYPES,
            coerce=int)


class RecommendForm(Form):
    email = TextField(
            validators=[validators.Email()], default='')
    link = TextField()
    type = TextField()
