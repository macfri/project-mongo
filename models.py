 # -*- coding: UTF-8 -*- #
import logging
import shutil
import os
import mongoengine as mng
import Image as Image_

from magic import Magic
from datetime import datetime
from cStringIO import StringIO

import settings

from libs.string_utils import slugify
from util import Auth, thumbnail

__all__ = ['Ubigeo', 'User', 'Contact', 'Specs', 'Characteristics',
           'Project', 'Image']



class Ubigeo(mng.Document):
    #departme_code = mng.StringField(default='00', required=True)
    city_code = mng.StringField(default='00', required=True)
    district_code = mng.StringField()
    name = mng.StringField()
    #count = mng.IntField(default=0)

    meta = {
        'collection': 'ubigeo',
        'indexes': ['city_code']
    }



class User(mng.Document, Auth):
    username = mng.StringField(
        required=True,
        unique=True,
        min_length=5,
        max_length=10
    )
    _password = mng.StringField(required=True)
    status = mng.BooleanField(default=True, required=True)

    meta = {
        'collection': 'users',
        'indexes': ['username']
    }


class Image(mng.Document):
    THUMB_SIZES = (
        (351, 280, 'large'),
        (277, 186, 'medium'),
        (165, 132, 'small'),
        (88, 69, 'tiny'),
    )

    EXTENSIONS = {
        'image/jpeg': '.jpg',
        'image/png': '.png',
        'image/gif': '.gif',
    }

    size = mng.ListField(mng.IntField())
    fext = mng.StringField()

    @property
    def tiny_url(self):
        return settings.IMAGE_URL % (self.id, 'tiny', self.fext)

    @property
    def small_url(self):
        return settings.IMAGE_URL % (self.id, 'small', self.fext)

    @property
    def medium_url(self):
        return settings.IMAGE_URL % (self.id, 'medium', self.fext)

    @property
    def large_url(self):
        return settings.IMAGE_URL % (self.id, 'large', self.fext)

    def save(self, raw_data=None, upload_path=settings.UPLOAD_PATH):
        """
        :raw_data String Buffer
        :upload_path Donde se guarda la imagen y sus "miniaturas"
        """
        if raw_data:
            super(Image, self).save()
            path = os.path.join(upload_path, str(self.id))
            fext = self.EXTENSIONS.get(
                    Magic(mime=True).from_buffer(raw_data))

            if not fext:
                raise Exception('Invalid image')

            try:
                os.makedirs(path)
            except OSError as exc:
                logging.error(exc)

            image = Image_.open(StringIO(raw_data))
            image.save(os.path.join(path, 'original%s' % fext))
            self.size = image.size
            self.fext = fext
            super(Image, self).save()

            for width, height, suffix in self.THUMB_SIZES:
                thumb = thumbnail(image, width, height, force=True,
                        crop=True, adjust_to_width=True)
                thumb.save(
                    os.path.join(path, '%s%s' % (suffix, fext))
                )

    def delete(self, upload_path=settings.UPLOAD_PATH, project=None):
        if project:
            project.images.remove(self)
            try:
                project.save()
            except mng.OperationError as exc:
                raise
            except Exception as exc:
                logging.error(exc)
        shutil.rmtree(os.path.join(upload_path, str(self.id)))
        super(Image, self).delete()


class Contact(mng.EmbeddedDocument):
    name = mng.StringField(required=True)
    email = mng.StringField(required=True)
    phone = mng.StringField(required=True)
    address = mng.StringField(required=True)


"""
class Specs(mng.EmbeddedDocument):
    builder = mng.StringField()
    num_available = mng.IntField()
    floor = mng.StringField()
    area_min = mng.FloatField()
    area_max = mng.FloatField()
    price_min = mng.FloatField()
    price_max = mng.FloatField()
    web = mng.StringField()


class Characteristics(mng.EmbeddedDocument):
    serviceroom = mng.BooleanField()
    gas = mng.BooleanField()
    swimming_pool = mng.BooleanField()
    closet = mng.BooleanField()
    laundry = mng.BooleanField()
    park_view = mng.BooleanField()
    terrace = mng.BooleanField()
    garden = mng.BooleanField()
    furnished = mng.BooleanField()  # lol no sabia que eso era amoblado
    kitchen_cabinet = mng.BooleanField()
"""



class News(mng.Document):

    title = mng.StringField(required=True)
    slug = mng.StringField(unique=True)
    description = mng.StringField(default='')
    image = mng.ReferenceField(Image)
    created_at = mng.DateTimeField(default=datetime.now, required=True)
    published_at = mng.DateTimeField(default=datetime.now, required=True)
    updated_at = mng.DateTimeField()
    status = mng.BooleanField(default=True, required=True)

    meta = {
        'collection': 'news',
        'indexes': [
            ('-published_at'),
            ('-created_at', '-updated_at')
        ]
    }

    @classmethod
    def _generate_slug(cls, title):
        _count = 2
        _slug = slugify(title)
        while cls.objects(slug=_slug).count() > 0:
            _slug = slugify(u'%s %s' % (title, _count))
            _count += 1
        return _slug

    def save(self, *args, **kwargs):
        self.updated_at = datetime.now()

        if not self.id:
            self.slug = self._generate_slug(self.title)

        super(News, self).save(*args, **kwargs)

    def delete(self):
        self.image.delete()
        super(News, self).delete()

class Project(mng.Document):

    """
    PRICE_TYPES = (
        (1, u'Soles'),
        (2, u'Dolares')
    )
    """

    TYPES = (
        (1, u'Casa'),
        (2, u'Departamento'),
        (9, u'Otros'),
    )

    title = mng.StringField(required=True)
    slug = mng.StringField(unique=True)
    #price = mng.FloatField(required=True)
    #address = mng.StringField(required=True)
    description = mng.StringField(default='')
    areas = mng.StringField()
    characteristics = mng.ListField(mng.StringField())
    information = mng.ListField()
    #city = mng.StringField(required=True)
    #district = mng.StringField(required=True)
    type = mng.IntField(required=True)
    proccess = mng.FloatField(required=True)
    banner = mng.ReferenceField(Image)
    images = mng.ListField(mng.ReferenceField(Image))
    #contact = mng.EmbeddedDocumentField(Contact)
    created_at = mng.DateTimeField(default=datetime.now, required=True)
    published_at = mng.DateTimeField(default=datetime.now, required=True)
    updated_at = mng.DateTimeField()
    status = mng.BooleanField(default=True, required=True)
    latitude = mng.StringField(required=True)
    longitude = mng.StringField(required=True)


    """
    @property
    def project_type(self):
        return dict(self.TYPES).get(self.type)
    """

    meta = {
        'collection': 'projects',
        'indexes': [
            ('-published_at'),
            ('-created_at', '-updated_at')
        ]
    }

    @classmethod
    def _generate_slug(cls, title):
        _count = 2
        _slug = slugify(title)
        while cls.objects(slug=_slug).count() > 0:
            _slug = slugify(u'%s %s' % (title, _count))
            _count += 1
        return _slug

    def save(self, *args, **kwargs):
        self.updated_at = datetime.now()

        #department_code = self.ubigeo.department_code
        #is_public = self.status

        #is_new = False
        #was_public = False
        #old_department_code = None

        #filters = dict(district_code='00', province_code='00',)

        if not self.id:
            self.slug = self._generate_slug(self.title)

        """
            is_new = True
        else:
            # instancia sin cambios
            instance = Project.objects.with_id(self.id)
            was_public = instance.status
            old_department_code = instance.ubigeo.department_code
            del instance

        if self.status:
            self.published_at = datetime.now()
        """

        """
        try:
            if (is_public and is_new) or \
               (not is_new and is_public and
                (not was_public or (department_code != old_department_code))):
                Ubigeo.objects(
                    department_code=department_code, **filters
                ).update_one(inc__count=1)

            if not is_new and (
                    (was_public and not is_public) or
                    (is_public and department_code != old_department_code)):
                Ubigeo.objects(
                    department_code=old_department_code, **filters
                ).update_one(dec__count=1)
        except Exception as exc:
            logging.debug(exc)
            raise
        else:
        """
        super(Project, self).save(*args, **kwargs)

    def delete(self):
        for image in self.images:
            image.delete()
        super(Project, self).delete()


"""
def insert_ubigeo():
    import re
    with open('ubigeo.sql') as f:
        for line in f:
            match = re.match(r'\d+\t(\d+)\t(\d+)\t(\d+)\t((\w|\s)+)\t.*', line)
            if match:
                Ubigeo(
                    department_code=match.group(1),
                    province_code=match.group(2),
                    district_code=match.group(3),
                    name=match.group(4),
                ).save()
"""


if __name__ == "__main__":
    db = mng.connect(**settings.MONGO)
    #db.users.remove()
    #db.ubigeo.remove()
    default_user = User()
    default_user.username = 'admin'
    default_user.password = '831134897aa7e7a65fd7f5adec84bb86'
    default_user.save()
    #insert_ubigeo()


    Ubigeo(city_code='01', district_code='00', name='Lima Norte').save()
    Ubigeo(city_code='01', district_code='01', name='Santa clara').save()
    Ubigeo(city_code='01', district_code='02', name='Carabayllo').save()
    Ubigeo(city_code='01', district_code='03', name='Santa Diego de Alcala').save()

    Ubigeo(city_code='02', district_code='00', name='Lima Sur').save()
    Ubigeo(city_code='02', district_code='01', name='x').save()
    Ubigeo(city_code='02', district_code='02', name='y').save()
    Ubigeo(city_code='02', district_code='03', name='z').save()

    Ubigeo(city_code='03', district_code='00', name='Lima Este').save()
    Ubigeo(city_code='03', district_code='01', name='a').save()
    Ubigeo(city_code='03', district_code='02', name='b').save()
    Ubigeo(city_code='03', district_code='03', name='c').save()
