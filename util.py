import os
import Image
import sys
import json
import bson
import datetime

from hashlib import sha1
from math import ceil
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE

__all__ = ['Auth', 'Paginator', 'Mail', 'thumbnail']


class Auth(object):

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        if not isinstance(password, (str, unicode, basestring)):
            raise Exception('Invalid type. Password must be a string.')

        hashed_password = password

        if isinstance(password, unicode):
            password_8bit = password.encode('UTF-8')
        else:
            password_8bit = password

        salt = sha1()
        salt.update(os.urandom(60))
        hash = sha1()
        hash.update(password_8bit + salt.hexdigest())
        hashed_password = salt.hexdigest() + hash.hexdigest()

        if not isinstance(hashed_password, unicode):
            hashed_password = hashed_password.decode('UTF-8')

        self._password = hashed_password

    def validate_password(self, password):
        hashed_pass = sha1()
        hashed_pass.update(password + self._password[:40])
        return self._password[40:] == hashed_pass.hexdigest()


class Paginator(object):

    def __init__(self, page, total_items, adjacents=2, per_page=10):
        self.page = page
        self.per_page = per_page
        self.total_items = total_items
        self.adjacents = adjacents

    @property
    def total_pages(self):
        return int(ceil(self.total_items / (self.per_page * 1.0)))

    @property
    def pages(self):
        if self.page > self.total_pages:
            pages = []
        elif self.total_pages <= 2 * self.adjacents:
            pages = range(1, self.total_pages + 1)
        else:
            if self.page <= self.adjacents:
                frm = 1
            else:
                if self.total_pages - self.page < self.adjacents:
                    frm = self.total_pages - 2 * self.adjacents
                else:
                    frm = self.page - self.adjacents

            inc = 2 * self.adjacents + 1
            pages = range(frm, frm + inc)

        return pages


class Mail(object):

    def __init__(self, subject='', sender='', to=None, cc=None, bcc=None):
        """
        @to list
        @cc list
        @bcc list
        """
        def to_list(val):
            if isinstance(val, list):
                return val
            else:
                return list(val) if val else []

        self._subject = subject
        self._sender = sender
        self._to = to_list(to)
        self._cc = to_list(cc)
        self._bcc = to_list(bcc)
        self._body = []

    @property
    def subject(self):
        return self._subject

    @subject.setter
    def subject(self, value=""):
        self._subject = value

    @property
    def sender(self):
        return self._sender

    @sender.setter
    def sender(self, value=""):
        self._sender = value

    @property
    def to(self):
        return self._to

    @to.setter
    def to(self, value=[]):
        value = value or []
        if isinstance(value, str):
            value = [value]
        self._to = list(value)

    @property
    def cc(self):
        return self._cc

    @cc.setter
    def cc(self, value):
        value = value or []
        if isinstance(value, str):
            value = [value]
        self._cc = list(value)

    @property
    def bcc(self):
        return self._bcc

    @bcc.setter
    def bcc(self, value):
        value = value or []
        if isinstance(value, str):
            value = [value]
        self._bcc = list(value)

    def add_body(self, content, type="plain"):
        self._body.append(MIMEText(content.encode("utf-8"), type, "utf-8"))

    def as_string(self):
        _mail = MIMEMultipart("alternative")
        _mail["Subject"] = self._subject
        _mail["From"] = self._sender
        _mail["To"] = COMMASPACE.join(self._to)
        if len(self._cc) > 0:
            _mail["CC"] = COMMASPACE.join(self._cc)
        for b in self._body:
            _mail.attach(b)
        return _mail.as_string()


def thumbnail(data, width, height, force=True, crop=True,
              adjust_to_width=True):
    """
    :force Si la imagen debe reducirse al tamano exacto proporcionado.

    :crop Si force es verdadero entonces indica que el thumb se creara a
    partir de centrar y cortar.

    :adjust_to_width Solo si force es False, si la imagen debe ajustarse al
    width proporcionado. Sirve en el caso que height sea mayor que el ancho
    y se necesita ajustar respecto a este ultimo. Usa el metodo thumbnail de
    PIL.
    """
    if isinstance(data, (str, unicode)):
        img = Image.open(data)
    else:
        img = data if force else data.copy()

    if img.mode != 'RGB':
        img = img.convert('RGB')

    src_width, src_height = img.size

    if not force:
        if adjust_to_width and src_height > src_width:
            height = sys.maxint
        img.thumbnail((width, height), Image.ANTIALIAS)
    else:
        src_ratio = float(src_width) / float(src_height)
        dst_width, dst_height = width, height
        dst_ratio = float(dst_width) / float(dst_height)

        if dst_ratio < src_ratio:
            crop_height = src_height
            crop_width = crop_height * dst_ratio
            x_offset = float(src_width - crop_width) / 2
            y_offset = 0
        else:
            crop_width = src_width
            crop_height = crop_width / dst_ratio
            x_offset = 0
            y_offset = float(src_height - crop_height) / 3

        if crop:
            img = img.crop((
                int(x_offset), int(y_offset),
                int(x_offset) + int(crop_width),
                int(y_offset) + int(crop_height)
            ))
        else:
            dst_height = int(dst_width / src_ratio)

        img = img.resize((dst_width, dst_height), Image.ANTIALIAS)

    return img


class JSONEncoder(json.JSONEncoder):

    def default(self, value):
        if isinstance(value, datetime.datetime):
            return value.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(value, bson.ObjectId):
            return str(value)
        super(JSONEncoder, self).default(value)
